from server.models.plan import Plan
from server.models.get_plan_params import GetPlanParams

class IRoutingProvider:
    """
    An informal interface for routing providers.
    """

    def get_plan(self, params: GetPlanParams) -> Plan:
        raise NotImplementedError()
