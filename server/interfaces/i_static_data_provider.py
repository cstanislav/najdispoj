from server.models.gtfs_folder import GTFSFolder
from server.models.osm_file import OSMFile


class IStaticDataProvider:
    """
    An informal interface for static data providers - not all functions have to be
    overridden by a provider class.
    """

    folder: GTFSFolder

    def load_data(self, osm_file: OSMFile) -> GTFSFolder | None:
        raise NotImplementedError()

    def get_folder(self) -> GTFSFolder:
        raise NotImplementedError()
