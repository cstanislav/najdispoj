from server.models.osm_file import OSMFile


class IGeodataProvider:
    """
    An informal interface for geodata providers.
    """

    def load_data(self) -> OSMFile | None:
        raise NotImplementedError()
