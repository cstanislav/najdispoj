from server.models.vehicle import Vehicle


class IDynamicDataProvider:
    """
    An informal interface for dynamic data providers.
    """

    def get_vehicles(self) -> dict[str, Vehicle]:
        raise NotImplementedError()
