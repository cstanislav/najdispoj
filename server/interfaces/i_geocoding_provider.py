from server.models.coords import Coords
from server.models.place import Place


class IGeocodingProvider:
    """
    An informal interface for geocoding providers - not all functions have to be
    overridden by a provider class.
    """

    def reverse_geocode(self, coords: Coords) -> Place:
        raise NotImplementedError()

    def autocomplete(self, query: str) -> list[Place]:
        raise NotImplementedError()
