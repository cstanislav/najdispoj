from server.models.coords import Coords


class BoundingBox:
    """
    Represents a coordinate (latitude, longitude) pair.

    Args:
        coords_min (Coords): Coords with smaller longitude and latitude
        coords_max (Coords): Coords with bigger longitude and latitude
    """

    def __init__(self, coords_min: Coords, coords_max: Coords):
        self.coords_min = coords_min
        self.coords_max = coords_max

    def __str__(self) -> str:
        """
        Returns a string of the following format:

        coords_min.lng,coords_min.lat,coords_max.lng,coords_max.lat
        """
        return f"{self.coords_min.lng},{self.coords_min.lat},{self.coords_max.lng},{self.coords_max.lat}"

    def add_coords(self, coords: Coords) -> None:
        """
        Expands the bounding box to include the given Coords.

        Args:
            coords (Coords): Coords to add
        """
        self.coords_min.lat = min(self.coords_min.lat, coords.lat)
        self.coords_min.lng = min(self.coords_min.lng, coords.lng)
        self.coords_max.lat = max(self.coords_max.lat, coords.lat)
        self.coords_max.lng = max(self.coords_max.lng, coords.lng)
