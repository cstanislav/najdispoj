from server.models.coords import Coords


class Stop:
    def __init__(self, name: str, coords: Coords, departure: int):
        self.name = name
        self.coords = coords
        self.departure = departure

    def to_json(self):
        return {
            "name": self.name,
            "coords": self.coords.to_json(),
            "departure": self.departure,
        }
