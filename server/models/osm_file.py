import os
import logging
import shutil

from server.models.bounding_box import BoundingBox


class OSMFile:
    # TODO add set_color_by_route_type
    def __init__(self, path: str):
        """
        Manager of an OSM file.

        Provides basic tools for working with the file.

        Args:
            path (str): The path to an .osm or .pbf file.
        """
        self.path = path
        self.label = "default_label"

    def crop(self, bounding_box: BoundingBox):
        # TODO is working?
        logging.info("🗺️  Cropping OSM data...")

        os.system(
            f"osmium extract --strategy complete_ways --bbox {bounding_box} {self.path} -o temp.osm --overwrite"
        )
        shutil.move("temp.osm", self.path)

        logging.info(f"✅ Done.")
        return self

    def filter(self, destination_osm_file: "OSMFile | None" = None):
        """Filters OSM data to keep only relevant tags for OTP routing."""
        logging.info("🗺️  Filtering OSM data...")

        output_path = destination_osm_file.path if destination_osm_file else self.path
        output_format = output_path.split(".")[-1]

        cmd = f"""osmium tags-filter {self.path} \
            w/highway \
            w/public_transport \
            w/railway \
            r/type=restriction \
            r/type=route \
            --overwrite \
            -o /mnt/data/osm/temp.osm -f {output_format},add_metadata=false"""

        logging.info(f"🏃 Running command {cmd}:")

        os.system(cmd)
        shutil.move("/mnt/data/osm/temp.osm", output_path)

        logging.info(f"✅ Done.")

        return destination_osm_file if destination_osm_file else self

    def cat(self, osm_file: "OSMFile"):
        logging.info(f"🗺️  Concatenating OSM data from {self.path} to {osm_file.path}...")

        os.system(f"osmium cat {self.path} -o {osm_file.path} --overwrite")

        logging.info("✅ Done.")
        return osm_file

    def set_label(self, label: str):
        self.label = label
        return self

    @staticmethod
    def merge(osm_files: list, destination: "OSMFile"):
        logging.info("🗺️  Merging OSM data...")

        paths = map(lambda f: f.path, osm_files)
        paths = " ".join(paths)
        os.system(f"osmium merge {paths} -o {destination.path} --overwrite")

        logging.info("✅ Done.")
        return destination
