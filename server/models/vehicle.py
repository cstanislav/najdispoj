from dataclasses import dataclass
from server.models.coords import Coords
from enum import Enum
import time

class VehicleType(Enum):
    BUS = "bus"
    TROLLEY = "trolley"
    TRAM = "tram"
    TRAIN = "train"
    SUBWAY = "subway"
    UNKNOWN = "unknown"

    @classmethod
    def from_gtfs(cls, type: str):
        """Creates a VehicleType from a GTFS route_type value."""
        match type:
            case "0" | 0:
                return cls.TRAM
            case "1" | 1:
                return cls.SUBWAY
            case "2" | 2:
                return cls.TRAIN
            case "3" | 3:
                return cls.BUS
            case "800" | 800 | "11" | 11:
                return cls.TROLLEY
            case _:
                return cls.UNKNOWN


@dataclass
class Vehicle:
    agency_id: str | None = None
    number: str | None = None
    routeShortName: str | None = None
    routeLongName: str | None = None
    coords: Coords | None = None
    bearing: float | None = None
    type: VehicleType = VehicleType.UNKNOWN
    lastUpdate: int = 0

    def changed(self):
        self.lastUpdate = int(time.time())

    def to_json(self):
        return {
            "agencyId": self.agency_id,
            "number": self.number,
            "routeShortName": self.routeShortName,
            "routeLongName": self.routeLongName,
            "coords": self.coords.to_json() if self.coords else None,
            "bearing": self.bearing,
            "type": self.type.value,
            "lastUpdate": self.lastUpdate,
        }

    def __str__(self):
        displayable = "✅" if self.is_displayable() else ""
        return f"Vehicle {self.to_json()} {displayable}"

    def is_displayable(self) -> bool:
        result = bool(
            self.coords
            and (self.routeShortName or self.routeLongName)
            and self.lastUpdate > time.time() - 300
        )
        return result


@dataclass
class DPBVehicle(Vehicle):
    order: int | None = None
    course: str | None = None
    direction: str | None = None
    service: str | None = None
    speed: int | None = None
    stop: str | None = None
    arrival_type: int | None = None
    stop_type: int | None = None

    def __init__(self):
        self.agency_id = "DPB"


@dataclass
class ZSRVehicle(Vehicle):
    delay: int | None = None

    def __init__(self):
        self.agency_id = "ZSR"
