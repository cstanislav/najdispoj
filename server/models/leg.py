from enum import Enum
from server.models.stop import Stop
from server.models.transit_mode import TransitMode


class Leg:
    """Represents a segment of an itinerary."""

    def __init__(
        self,
        mode: TransitMode,
        duration: int,
        distance: int,
        path: str,
        departure: int,
        arrival: int,
        number: str,
        short_name: str,
        headsign: str,
        stops: list[Stop],
        agency_id: str | None = None,
    ):
        self.mode = mode
        self.duration = duration
        self.distance = distance
        self.path = path
        self.departure = departure
        self.arrival = arrival
        self.number = number
        self.short_name = short_name
        self.headsign = headsign
        self.stops = stops
        self.agency_id = agency_id
        self.waiting_duration: int = 0
        self.timeline_duration: int | None = None

    def get_total_duration(self) -> int:
        return self.duration + self.waiting_duration

    def compute_timeline_duration(self, max_duration: int) -> None:
        self.timeline_duration = int(round(
            self.get_total_duration() / max_duration * 100, 2
        ))

    def set_waiting_duration(self, duration: int) -> None:
        self.waiting_duration = duration

    def to_json(self) -> dict:
        """
        Returns an object, which can be automatically converted to JSON by
        FastAPI and sent to a client in JSON format.
        """
        return {
            "mode": self.mode.value,
            "duration": self.duration,
            "distance": self.distance,
            "path": self.path,
            "departure": self.departure,
            "arrival": self.arrival,
            "number": self.number,
            "shortName": self.short_name,
            "headsign": self.headsign,
            "stops": list(map(lambda stop: stop.to_json(), self.stops)),
            "agencyId": self.agency_id,
            "timelineDuration": self.timeline_duration,
            "waitingDuration": self.waiting_duration,
            "totalDuration": self.get_total_duration(),
        }
