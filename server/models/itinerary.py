import math
from server.models.leg import Leg
from server.models.transit_mode import TransitMode


class Itinerary:
    """Represents an itinerary in a plan."""

    def __init__(self, duration: int, departure: int, arrival: int, legs: list[Leg]):
        self.duration = duration
        self.departure = departure
        self.arrival = arrival
        self.legs = legs
        self.timeline_duration: int | None = None

        self.compute_waiting()

    def compute_timeline(self, max_duration: int) -> None:
        """Compute timeline duration for all legs of the itinerary."""
        self.timeline_duration = int(round(self.duration / max_duration * 100, 2))

        for leg in self.legs:
            leg.compute_timeline_duration(max_duration)

    def compute_waiting(self) -> None:
        """Compute waiting duration for all legs of the itinerary."""
        leg_len = len(self.legs)

        for index, leg in enumerate(self.legs):
            if index < leg_len - 2 and index != 0:
                next_leg = self.legs[index + 1]
                if leg.mode == TransitMode.WALK:
                    waiting_duration = math.floor(
                        (int(next_leg.departure) - int(leg.arrival)) / 1000
                    )
                else:
                    waiting_duration = math.floor(
                        (
                            int(next_leg.departure)
                            - int(leg.stops[-1].departure)
                        )
                        / 1000
                    )

                if (
                    waiting_duration > 60
                    and leg.mode == TransitMode.WALK
                    and next_leg.mode != TransitMode.WALK
                ):
                    waiting_duration -= 60

                leg.set_waiting_duration(waiting_duration)

    def to_json(self) -> dict:
        """
        Returns an object, which can be automatically converted to JSON by
        FastAPI and sent to a client in JSON format.
        """
        return {
            "duration": self.duration,
            "departure": self.departure,
            "arrival": self.arrival,
            "legs": list(map(lambda leg: leg.to_json(), self.legs)),
            "timelineDuration": self.timeline_duration,
        }
