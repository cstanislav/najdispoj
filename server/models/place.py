from typing import TypedDict
from enum import Enum
from dataclasses import dataclass, field
from server.models.coords import Coords
from server.models.transit_mode import TransitMode


class PlaceName(TypedDict):
    primary: str
    secondary: str


class PlaceType(Enum):
    STREET = "street"
    SHOP = "shop"
    INSTITUTION = "institution"
    STOP = "stop"
    TOWN = "town"
    BUILDING = "building"
    OTHER = "other"


@dataclass
class Place:
    name: PlaceName
    coords: Coords
    type_: PlaceType
    # Only populated for PlaceType.STOP
    modes: list[TransitMode] = field(default_factory=list)
    stop_ids: list[str] | None = None


    def to_json(self) -> dict:
        """
        Return an object, which can be automatically converted to JSON by
        FastAPI and sent to a client in JSON format.
        """
        return {
            "name": self.name,
            "coords": self.coords.to_json(),
            "type": self.type_.value,
            "modes": self.modes,
            "stopIds": self.stop_ids,
        }

    def __str__(self) -> str:
        return (
            self.name["primary"]
            + ", "
            + self.name["secondary"]
            + ": "
            + str(self.coords)
            + " ("
            + ", ".join(m.value for m in self.modes)
            + ")"
        )

    @staticmethod
    def sort_places(places: list) -> None:
        """Sorts a list of Places in-place."""

        def sortFunc(place: Place):
            typeRatings = {
                PlaceType.STOP: 0,
                PlaceType.INSTITUTION: 1,
                PlaceType.SHOP: 2,
                PlaceType.BUILDING: 3,
                PlaceType.STREET: 4,
                PlaceType.OTHER: 5,
            }

            if place and place.type_ in typeRatings:
                return typeRatings[place.type_]
            else:
                return 100

        places.sort(key=sortFunc)
