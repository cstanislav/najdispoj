import pandas
import os
import logging
import zipfile
import glob
import csv
import unicodedata
import shutil
from server.models.osm_file import OSMFile
from pyproj import Transformer

import server.config as config


class GTFSFolder:
    def __init__(self, path: str):
        """
        Manager of a GTFS folder.

        Provides basic tools for working with GTFS data.

        Args:
            path (str): The path to GTFS data folder.
        """
        self.path = path
        self.label = "default_label"

    def load_zip(self, path: str):
        logging.info(f"🚉 Loading zip from path {path} to {self.path}...")

        with zipfile.ZipFile(path, "r") as file:
            file.extractall(self.path)

        validity = self.get_feed_validity()
        logging.info(
            f"✅ Done. Feed valid from {validity[0]} until {validity[1]}."
        )
        return self

    def create_feed_info(self, url: str):
        """
        Creates a feed_info.txt file with the label of the GTFS data provider.
        """
        logging.info("🚉 Creating feed_info.txt...")

        with open(self.path + "/feed_info.txt", "w") as file:
            file.write(
                "feed_publisher_name,feed_publisher_url,feed_lang,feed_start_date,feed_end_date,feed_version,feed_contact_email,feed_contact_url,default_lang\n"
            )
            file.write(f"{self.label},{url},sk,,,,,")

        logging.info("✅ Done.")
        return self

    def repair_feed_info(self, url: str):
        """
        Remove accents from feed info - accents cause an error in
        OpenTripPlanner.

        Adds URL of feed provider (required by OpenTripPlanner).
        """
        logging.info("🚉 Repairing feed info...")

        with (
            open(self.path + "/feed_info.txt", "r") as file,
            open(self.path + "/feed_info.txt.tmp", "w+") as tmp,
        ):
            reader = csv.DictReader(file)
            fieldnames = (
                list(reader.fieldnames) if reader.fieldnames else list()
            )
            writer = csv.DictWriter(tmp, fieldnames=fieldnames)

            writer.writeheader()
            for row in reader:
                # Remove accents
                row["feed_publisher_name"] = (
                    unicodedata.normalize("NFKD", row["feed_publisher_name"])
                    .encode("ascii", "ignore")
                    .decode("ascii")
                )
                # Set URL
                row["feed_publisher_url"] = url
                writer.writerow(row)

        os.remove(self.path + "/feed_info.txt")
        os.rename(
            self.path + "/feed_info.txt.tmp",
            self.path + "/feed_info.txt",
        )

        logging.info("✅ Done.")
        return self

    def get_feed_validity(self):
        """
        Returns the start and end date of the feed.

        Try to open feed_info.txt - if it exists and the dates are not empty, return them.
        Try to open calendar.txt - if it exists, find the minimum of start_date and maximum of end_date using pandas and return them.
        Try to open calendar_dates.txt - if it exists, find the minimum of date and maximum of date using pandas and return them.
        Otherwise return empty strings.
        """
        try:
            with open(self.path + "/feed_info.txt", "r") as file:
                reader = csv.DictReader(file)
                row = next(reader)  # Contains only a single line
                if row["feed_start_date"] and row["feed_end_date"]:
                    return row["feed_start_date"], row["feed_end_date"]
        except FileNotFoundError:
            pass

        try:
            with open(self.path + "/calendar.txt", "r") as file:
                df = pandas.read_csv(file)
                return df["start_date"].min(), df["end_date"].max()
        except FileNotFoundError:
            pass

        try:
            with open(self.path + "/calendar_dates.txt", "r") as file:
                df = pandas.read_csv(file)
                return df["date"].min(), df["date"].max()
        except FileNotFoundError:
            pass

        return "", ""

    def repair_stops(self):
        """
        The specified parent stations are not defined. To solve the problem we
        can simply remove them.
        """
        # TODO check whether it exists before removing?
        logging.info("🚉 Removing parent stops...")

        with (
            open(self.path + "/stops.txt", "r") as file,
            open(self.path + "/stops.txt.tmp", "w") as tmp,
        ):
            reader = csv.DictReader(file)
            fieldnames = (
                list(reader.fieldnames) if reader.fieldnames else list()
            )
            writer = csv.DictWriter(tmp, fieldnames=fieldnames)

            writer.writeheader()
            for row in reader:
                row["parent_station"] = ""
                writer.writerow(row)

        os.remove(self.path + "/stops.txt")
        os.rename(self.path + "/stops.txt.tmp", self.path + "/stops.txt")

        logging.info("✅ Done.")
        return self

    def replace_route_types(self, type_from: str, type_to: str):
        """
        800 is the trolleybus route_type in Extended GTFS Route Types:
        https://developers.google.com/transit/gtfs/reference/extended-route-types
        11 is the route_type according to these docs:
        https://developers.google.com/transit/gtfs/reference#routestxt
        OTP 1.5.0 refuses to build graph unless this gets replaced
        """
        # TODO look deeper into this issue
        logging.info("🚉 Repairing routes...")

        with (
            open(self.path + "/routes.txt", "r") as file,
            open(self.path + "/routes.txt.tmp", "w") as tmp,
        ):
            reader = csv.DictReader(file)
            fieldnames = (
                list(reader.fieldnames) if reader.fieldnames else list()
            )
            writer = csv.DictWriter(tmp, fieldnames=fieldnames)

            writer.writeheader()
            for row in reader:
                if row["route_type"] == type_from:
                    row["route_type"] = type_to
                writer.writerow(row)

        os.rename(self.path + "/routes.txt", self.path + "routes.txt.old")
        os.rename(self.path + "/routes.txt.tmp", self.path + "/routes.txt")

        logging.info("✅ Done.")
        return self

    def replace_agency_id(self, old_id: str, new_id: str):
        """
        Replace agency_id in all files
        """
        logging.info(
            f"🚉 Replacing agency_id {old_id} with {new_id}..."
        )

        for file_name in os.listdir(self.path):
            if file_name.endswith(".txt"):
                with (
                    open(f"{self.path}{file_name}", "r") as file,
                    open(f"{self.path}{file_name}.tmp", "w") as tmp,
                ):
                    reader = csv.DictReader(file)
                    fieldnames = (
                        list(reader.fieldnames) if reader.fieldnames else list()
                    )
                    writer = csv.DictWriter(tmp, fieldnames=fieldnames)

                    writer.writeheader()
                    for row in reader:
                        # If row contains agency_id key and it's equal to old_agency_id
                        if (
                            "agency_id" in row
                            and row["agency_id"] == old_id
                        ):
                            row["agency_id"] = new_id
                        writer.writerow(row)

                os.remove(f"{self.path}{file_name}")
                os.rename(
                    f"{self.path}{file_name}.tmp", f"{self.path}{file_name}"
                )

        logging.info("✅ Done.")
        return self

    def revert_routes(self):
        logging.info("🚉 Reverting routes...")

        os.remove(self.path + "routes.txt")
        os.rename(self.path + "routes.txt.old", self.path + "routes.txt")

        logging.info("✅ Done.")
        return self

    def convert_stop_coords(self):
        """
        Convert stop coordinates from epsg:5514 coord system to epsg:4326
        """
        logging.info("🚉 Repairing stop coordinates...")

        with (
            open(self.path + "stops.txt", "r") as file,
            open(self.path + "stops.txt.tmp", "w") as tmp,
        ):
            reader = csv.DictReader(file)
            fieldnames = (
                list(reader.fieldnames) if reader.fieldnames else list()
            )
            writer = csv.DictWriter(tmp, fieldnames=fieldnames)

            writer.writeheader()
            for row in reader:
                transformer = Transformer.from_crs("EPSG:5514", "EPSG:4326")
                new_coords = transformer.transform(
                    row["stop_lat"], row["stop_lon"]
                )
                row["stop_lat"] = new_coords[0]
                row["stop_lon"] = new_coords[1]
                writer.writerow(row)

        os.remove(self.path + "/stops.txt")
        os.rename(self.path + "/stops.txt.tmp", self.path + "/stops.txt")

        logging.info("✅ Done.")
        return self

    def generate_shapes(self, osm_file: OSMFile):
        """Create shape files using Pfaedle."""
        logging.info("🚉 Generating shapes...")

        cmd = (
            f"{config.pfaedle_path} -D --inplace -x {osm_file.path} {self.path}"
        )

        logging.info(f"🏃 Running command: {cmd}")
        os.system(cmd)

        logging.info("✅ Done.")
        return self

    def zip(self, zip_path: str | None = None) -> str:
        """
        Zip GTFS data and return the path to the zip file.
        """

        if zip_path is None:
            zip_path = f"{self.path}gtfs_{self.label}.zip"
        
        if not zip_path.endswith(".zip"):
            zip_path += ".zip"

        logging.info(f"🚉 Zipping GTFS data from {self.path} to {zip_path}...")

        shutil.make_archive(os.path.splitext(zip_path)[0], "zip", self.path)

        logging.info("✅ Done.")
        return zip_path

    def copy(self, path: str):
        """
        Copies GTFS data to <path> and returns the copy's GTFSFolder object.
        """
        logging.info("🚉 Copying GTFS data from %s to %s...", self.path, path)

        shutil.copytree(self.path, path, dirs_exist_ok=True)

        logging.info("✅ Done.")
        return GTFSFolder(path).set_label(f"{self.label} - copy")

    def set_label(self, label: str):
        """
        The label used e.g. when naming related zip files.

        Args:
            label (str): ASCII string
        """
        logging.info(f"🚌 Setting label to {label}")

        self.label = label

        logging.info("✅ Done.")
        return self

    def get_routes(self):
        """Returns a list of routes in the GTFS data"""
        # Load routes to an attribute for faster access (avoiding reading the file after every request)
        try:
            with open(self.path + "/routes.txt", "r") as file:
                reader = csv.DictReader(file)
                return list(reader)
        except FileNotFoundError:
            return []

    def trim_empty_lines(self):
        """
        Remove empty lines from all .txt files in the GTFS folder. Keep a single \n at the end.
        """

        logging.info("🚉 Trimming empty lines...")

        for file_name in glob.glob(self.path + "/*.txt"):
            with open(file_name, "r") as file:
                lines = file.readlines()
            with open(file_name, "w", newline="\r\n") as file:
                file.writelines([line for line in lines if line.strip()])

        logging.info("✅ Done.")
        return self

    def remove_transfers(self):
        """
        Remove transfers.txt file from the GTFS folder if it exists.
        """

        logging.info("🚉 Removing transfers file...")

        try:
            os.remove(self.path + "/transfers.txt")
        except FileNotFoundError:
            pass

        logging.info("✅ Done.")
        return self

    def add_feed_id(self, feed_id: str):
        """
        Add feed_id column to feed_info.txt file.
        """
        logging.info("🚉 Adding feed_id...")

        with (
            open(self.path + "/feed_info.txt", "r") as file,
            open(self.path + "/feed_info.txt.tmp", "w") as tmp,
        ):
            reader = csv.DictReader(file)
            fieldnames = (
                list(reader.fieldnames) if reader.fieldnames else list()
            )
            fieldnames.append("feed_id")
            writer = csv.DictWriter(tmp, fieldnames=fieldnames)

            writer.writeheader()
            for row in reader:
                row["feed_id"] = feed_id
                writer.writerow(row)

        os.remove(self.path + "/feed_info.txt")
        os.rename(
            self.path + "/feed_info.txt.tmp", self.path + "/feed_info.txt"
        )

        logging.info("✅ Done.")
        return self