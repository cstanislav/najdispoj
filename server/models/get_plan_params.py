from typing import TypedDict
from server.models.coords import Coords

class GetPlanParams(TypedDict):
    origin: Coords
    destination: Coords
    arrive_by: bool
    date: str
    time: str
    max_transfers: int
    walk_reluctance: int
    walk_speed: int
    optimize: str