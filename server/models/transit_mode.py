from enum import Enum

class TransitMode(Enum):
    # Mostly aligned with OpenTripPlanner TransitMode enum
    WALK = "WALK"
    RAIL = "RAIL"
    COACH = "COACH"
    SUBWAY = "SUBWAY"
    BUS = "BUS"
    TRAM = "TRAM"
    FERRY = "FERRY"
    TROLLEYBUS = "TROLLEYBUS"
    OTHER = "OTHER"
    UNKNOWN = "UNKNOWN"
