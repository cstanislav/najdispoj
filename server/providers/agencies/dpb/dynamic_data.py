from threading import Thread
from server.models.coords import Coords
from server.models.vehicle import DPBVehicle, VehicleType, Vehicle
from server.models.gtfs_folder import GTFSFolder
import socket
from server.interfaces.i_dynamic_data_provider import IDynamicDataProvider
from .messages import (
    MessageType,
    StatusMessage,
    StopCourseMessage,
    StopDirectionMessage,
    GPSMessage,
)
import logging


class DPBDynamicDataProvider(IDynamicDataProvider):
    """
    Provider of dynamic vehicle data from DPB (Radiopol).

        Args:
            address (str): Address to listen on.
            port (int): Port to listen on.
    """

    def __init__(
        self,
        address: str,
        port: int,
        gtfs_folder: GTFSFolder,
    ):
        self._address = address
        self._port = port
        self._gtfs_folder = gtfs_folder
        self._vehicles: dict[str, Vehicle] = {}

        self.routes = self._gtfs_folder.get_routes()

        self.start_listener()

    def start_listener(self):
        def listen():
            buffer_size = 1024

            while True:
                try:
                    data, _ = self._socket.recvfrom(buffer_size)
                    self.handle_packet(data)
                except socket.error as exc:
                    logging.error("socket.error:", exc)

        logging.info("Starting dynamic data listener...")

        self._socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self._socket.bind((self._address, self._port))
        self.listener = Thread(target=listen)
        # Without the following line, the listener thread cannot be killed using Ctrl+C
        self.listener.setDaemon(True)
        self.listener.start()

    def get_vehicles(self) -> dict[str, Vehicle]:
        """Returns a dictionary of vehicles."""

        return self._vehicles

    def get_add_vehicle(self, number: str) -> DPBVehicle:
        """
        Gets vehicle by its number; if it doesn't exist, creates a new vehicle,
        adds it to self._vehicles and returns it.
        """
        if number in self._vehicles:
            return self._vehicles[number]
        else:
            new_vehicle = DPBVehicle()
            # Since DPB only uses ints as vehicle numbers, we can use that representation within this class, but we need to convert it to string for the rest of the app
            new_vehicle.number = str(number)
            self.add_vehicle(new_vehicle)
            return new_vehicle

    def add_vehicle(self, vehicle) -> None:
        self._vehicles[vehicle.number] = vehicle

    def get_message_type(self, data) -> MessageType:
        match data[4:6]:
            case "03":
                return MessageType.STATUS
            case "0b":
                return MessageType.STOPCOURSE
            case "0c":
                return MessageType.STOPDIRECTION
            case "83":
                return MessageType.GPS
            case _:
                return MessageType.UNKNOWN

    def handle_packet(self, data) -> None:
        hexdata = data.hex()
        match self.get_message_type(hexdata):
            case MessageType.STATUS:
                status_msg = StatusMessage(hexdata)
                # logging.debug(status_msg)
                if status_msg.line:
                    vehicle = self.get_add_vehicle(status_msg.vehicle_number)
                    vehicle.routeShortName = status_msg.line
                    vehicle.type = self.get_type_by_line(status_msg.line)
                    vehicle.order = status_msg.order
                    vehicle.changed()

            case MessageType.STOPCOURSE:
                stop_course_msg = StopCourseMessage(hexdata)
                # logging.debug(stop_course_msg)
                if stop_course_msg.line:
                    vehicle = self.get_add_vehicle(stop_course_msg.vehicle_number)
                    vehicle.routeShortName = stop_course_msg.line
                    vehicle.type = self.get_type_by_line(stop_course_msg.line)
                    vehicle.course = str(stop_course_msg.course)
                    vehicle.service = stop_course_msg.service
                    vehicle.stop = str(stop_course_msg.stop)
                    vehicle.arrival_type = stop_course_msg.arrival_type
                    vehicle.stop_type = stop_course_msg.stop_type
                    vehicle.changed()

            case MessageType.STOPDIRECTION:
                stop_direction_msg = StopDirectionMessage(hexdata)
                # logging.debug(stop_direction_msg)
                if stop_direction_msg.line:
                    vehicle = self.get_add_vehicle(stop_direction_msg.vehicle_number)
                    vehicle.routeShortName = stop_direction_msg.line
                    vehicle.type = self.get_type_by_line(stop_direction_msg.line)
                    vehicle.direction = stop_direction_msg.direction
                    vehicle.service = stop_direction_msg.service
                    vehicle.stop = str(stop_direction_msg.stop)
                    vehicle.arrival_type = stop_direction_msg.arrival_type
                    vehicle.stop_type = stop_direction_msg.stop_type
                    vehicle.changed()

            case MessageType.GPS:
                try:
                    gps_msg: GPSMessage = GPSMessage(hexdata)
                    # logging.debug(gps_msg)
                    vehicle = self.get_add_vehicle(gps_msg.vehicle_number)
                    vehicle.coords = Coords(gps_msg.latitude, gps_msg.longitude)
                    vehicle.speed = gps_msg.velocity
                    vehicle.bearing = gps_msg.bearing
                    vehicle.changed()

                except ValueError:
                    # TODO probably just a packet with all relevances turned off
                    # logging.info(
                    #     "Received a corrupt packet, unable to create a datetime."
                    # )
                    # print("ERROR:", hexdata)
                    return
            case MessageType.UNKNOWN:
                logging.debug("UNKNOWN:", hexdata)

    def get_type_by_line(self, line: str) -> VehicleType:
        for route in self.routes:
            if route["route_short_name"] == line:
                return VehicleType.from_gtfs(route["route_type"])
        return VehicleType.UNKNOWN
