import logging
import os
import requests

from server.interfaces.i_static_data_provider import IStaticDataProvider
from server.models.gtfs_folder import GTFSFolder
from server.models.osm_file import OSMFile


class DPMLJStaticDataProvider(IStaticDataProvider):
    """
    Provider of raw GTFS data of DPMLJ (Dopravní podnik měst Liberce a Jablonce nad Nisou, a. s.).
    """

    def __init__(self, path, label="DPMLJ"):
        self.folder = GTFSFolder(path).set_label(label)
        self.label = label

    def load_data(self, osm_file: OSMFile) -> GTFSFolder | None:
        """Download GTFS data to the given path."""
        gtfs_url = "http://www.dpmlj.cz/gtfs.zip"
        zip_path = f"{self.folder.path}gtfs.zip"

        logging.info(f"🚉DPMLJ Downloading GTFS data from {gtfs_url}...")
        try:
            os.makedirs(os.path.dirname(zip_path), exist_ok=True)
            with open(zip_path, "wb+") as file:
                # urllib.urlretrieve can't be used due to SSL verification
                response = requests.get(gtfs_url)
                file.write(response.content)
        except requests.exceptions.RequestException as e:
            logging.info(f"❌ Error: {e}")
            logging.info("❌ Download has failed.")
            return None

        logging.info("✅ Done.")

        return (
            self.folder
            .load_zip(zip_path)
            .create_feed_info("https://www.dpmlj.cz/")
            .add_feed_id("DPMLJ")
        )
