import logging
import os
import urllib.request
import urllib.error
import requests
from io import StringIO
from bs4 import BeautifulSoup

from server.interfaces.i_static_data_provider import IStaticDataProvider
from server.models.gtfs_folder import GTFSFolder
from server.models.osm_file import OSMFile


class IDSJMKStaticDataProvider(IStaticDataProvider):
    """
    Provider of raw GTFS data of IDS JMK (Integrovaný dopravní systém Jihomoravského kraje)
    """

    def __init__(self, path, label="IDSJMK"):
        self.folder = GTFSFolder(path).set_label(label)
        self.label = label

    def load_data(self, osm_file: OSMFile) -> GTFSFolder | None:
        """Download GTFS data to the given path."""
        gtfs_url = f"https://www.arcgis.com/sharing/rest/content/items/379d2e9a7907460c8ca7fda1f3e84328/data"
        zip_path = f"{self.folder.path}gtfs.zip"

        logging.info(f"🚉IDSJMK Downloading GTFS data from {gtfs_url}...")
        try:
            os.makedirs(os.path.dirname(zip_path), exist_ok=True)
            with open(zip_path, "wb+") as file:
                # urllib.urlretrieve didn't work (due to SSL verification)
                response = requests.get(gtfs_url)
                file.write(response.content)
        except requests.exceptions.RequestException as e:
            logging.info(f"❌ Error: {e}")
            logging.info("❌ Download has failed.")
            return None

        logging.info("✅ Done.")

        return (
            self.folder
            .load_zip(zip_path)
            .create_feed_info("https://www.idsjmk.cz/")
            .add_feed_id(self.label)
            .replace_route_types("11", "3")
            .generate_shapes(osm_file)
            .revert_routes()
            .replace_route_types("11", "800")
        )
