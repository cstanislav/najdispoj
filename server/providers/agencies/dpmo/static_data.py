import logging
import requests
from bs4 import BeautifulSoup
import os

from server.interfaces.i_static_data_provider import IStaticDataProvider
from server.models.gtfs_folder import GTFSFolder
from server.models.osm_file import OSMFile

class DPMOStaticDataProvider(IStaticDataProvider):
    """
    Provider of raw GTFS data from DPMO (Dopravní podnik města Olomouce, a.s.)
    """

    def __init__(self, path, label="DPMO"):
        self.folder = GTFSFolder(path).set_label(label)
        self.label = label

    def download_data(self, path: str) -> GTFSFolder | None:
        """Download GTFS data to the given path."""
        

    def load_data(self, osm_file: OSMFile) -> GTFSFolder | None:
        page_url = "https://www.dpmo.cz/informace-pro-cestujici/jizdni-rady/jizdni-rady-gtfs/"

        html_raw = requests.get(page_url).content
        parser = BeautifulSoup(html_raw, "html.parser")

        a_element = parser.select_one(".hlavni-obsah .dokumenty .nazev a")

        if not a_element:
            logging.error("No GTFS data found.")
            return None

        a_href = a_element.attrs["href"].rstrip("?")
        gtfs_url = f"https://www.dpmo.cz{a_href}"
        zip_path = f"{self.folder.path}gtfs.zip"

        logging.info(f"🚉DPMO Downloading GTFS data from {gtfs_url}...")
        try:
            os.makedirs(os.path.dirname(zip_path), exist_ok=True)
            with open(zip_path, "wb+") as file:
                # urllib.urlretrieve can't be used due to SSL verification
                response = requests.get(gtfs_url)
                file.write(response.content)
        except requests.exceptions.RequestException as e:
            logging.info(f"❌ Error: {e}")
            logging.info("❌ Download has failed.")
            return None

        logging.info("✅ Done.")

        return (
            self.folder
            .load_zip(zip_path)
            .repair_feed_info("https://dpmo.cz/")
            .generate_shapes(osm_file)
        )
