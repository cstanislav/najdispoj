import logging
import os
import requests

from server.interfaces.i_static_data_provider import IStaticDataProvider
from server.models.gtfs_folder import GTFSFolder
from server.models.osm_file import OSMFile


class ZSRStaticDataProvider(IStaticDataProvider):
    """
    Provider of raw GTFS data from ŽSR (Železnice Slovenskej republiky).
    """

    def __init__(self, path, label="ZSR"):
        self.folder = GTFSFolder(path).set_label(label)
        self.label = label

    def load_data(self, osm_file: OSMFile) -> GTFSFolder | None:
        """Download GTFS data to the given path."""
        gtfs_url = f"https://www.zsr.sk/files/pre-cestujucich/cestovny-poriadok/gtfs/gtfs.zip"
        zip_path = f"{self.folder.path}gtfs.zip"

        logging.info(f"🚉ZSR Downloading GTFS data from {gtfs_url}...")
        try:
            os.makedirs(os.path.dirname(zip_path), exist_ok=True)
            with open(zip_path, "wb+") as file:
                # urllib.urlretrieve didn't work (due to SSL verification)
                response = requests.get(gtfs_url)
                file.write(response.content)
        except requests.exceptions.RequestException as e:
            logging.info(f"❌ Error: {e}")
            logging.info("❌ Download has failed.")
            return None

        logging.info("✅ Done.")

        return (
            self.folder
            .load_zip(zip_path)
            .generate_shapes(osm_file)
        )
