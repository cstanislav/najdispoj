import json
import logging
import requests
from apscheduler.schedulers.background import BackgroundScheduler

from server.interfaces.i_dynamic_data_provider import IDynamicDataProvider
from server.models.coords import Coords
from server.models.vehicle import Vehicle, VehicleType

QUERY_FILE = "server/queries/opentripplanner2_vehicles.graphql"

class OTP2DynamicDataProvider(IDynamicDataProvider):
    """
    Provider of dynamic vehicle data from OpenTripPlanner 2.
    Requires a running OTP2 instance with GraphQL API enabled.
    """

    def __init__(self, address: str):
        self._address = address
        self._vehicles: dict[str, Vehicle] = {}

        with open(QUERY_FILE, "r") as file:
            self.query = file.read()
        
        self.start_querying()

    def start_querying(self):
        def query():
            def handle_response(response):
                patterns = response["data"]["patterns"]
                for pattern in patterns:
                    agency_id = pattern["route"]["agency"]["gtfsId"].split(":")[0]
                    routeShortName = pattern["route"]["shortName"]
                    routeLongName = pattern["route"]["longName"]
                    for vehicle in pattern["vehiclePositions"]:
                        self._vehicles[vehicle["vehicleId"]] = Vehicle(
                            agency_id,
                            vehicle["label"],
                            routeShortName,
                            routeLongName,
                            Coords(vehicle["lat"], vehicle["lon"]),
                            vehicle["heading"],
                            VehicleType.from_gtfs(pattern["route"]["type"]),
                            vehicle["lastUpdated"],
                        )
            data = {"query": self.query}
            try:
                response = requests.post(
                    f"{self._address}/otp/routers/default/index/graphql",
                    json=data,
                    headers={"Content-Type": "application/json"},  # Would return 415 otherwise
                )
            except requests.exceptions.ConnectionError as e:
                # logging.error(f"Connection error: {e}. If OTP2 is initializing, this message can be ignored.")
                return

            logging.info(f"OpenTripPlanner POST url: {response.url}")
            logging.info(f"Data: {data}")

            response = json.loads(response.content)
            handle_response(response)
        
        scheduler = BackgroundScheduler()
        scheduler.add_job(query, "interval", seconds=20)
        scheduler.start()
        logging.getLogger('apscheduler.executors.default').setLevel(logging.WARNING)


    def get_vehicles(self) -> dict[str, Vehicle]:
        """Returns a dictionary of vehicles."""

        return self._vehicles