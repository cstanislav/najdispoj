import requests

from server.interfaces.i_geocoding_provider import IGeocodingProvider
from server.models.place import Place, PlaceType, PlaceName
from server.models.coords import Coords
from server.models.bounding_box import BoundingBox

import server.config as cfg


class PhotonGeocodingProvider(IGeocodingProvider):
    """
    Provider of geocoding data from Komoot Photon
    https://github.com/komoot/photon
    """

    def __init__(self, address):
        # IP address and port of a Photon API instance.
        # (Without the trailing '/')
        self.address = address

    def reverse_geocode(self, coords: Coords) -> Place:
        params = {"lat": str(coords.lat), "lon": str(coords.lng)}

        data = requests.get(self.address + "/reverse", params=params).json()

        return Place(self.get_name_from_geojson(data["features"][0]), coords, PlaceType.OTHER)

    def autocomplete(self, query: str) -> list[Place]:
        bbox = BoundingBox(
            Coords.from_json(cfg.area["bbox"][0]),
            Coords.from_json(cfg.area["bbox"][1]),
        )
        center = Coords.from_json(cfg.area["center"])

        params: dict = {
            "q": query,
            "limit": 10,
            "bbox": str(bbox),
            "lat": center.lat,
            "lon": center.lng,
            "location_bias_scale": 2,
            "osm_tag": [
                "!boundary",
                ":!unclassified",
                ":!town",
                ":!neighbourhood",
            ],
        }

        data = requests.get(self.address + "/api", params=params).json()

        used_ids: list[int] = []
        results: list[Place] = []

        # print("Data:", json.dumps(data, indent=2))
        features = data["features"]
        for place in features:
            coords = Coords(
                place["geometry"]["coordinates"][1],
                place["geometry"]["coordinates"][0],
            )

            place_id = place["properties"]["osm_id"]

            if place_id in used_ids:
                continue

            used_ids.append(place_id)

            results.append(
                Place(self.get_name_from_geojson(place), coords, self.get_type_from_geojson(place))
            )

        return results
    
    @staticmethod
    def get_name_from_geojson(obj: dict) -> PlaceName:
        name: PlaceName = {"primary": "", "secondary": ""}

        props = obj["properties"]

        if "name" in props:
            name["primary"] = props["name"]
        elif "street" in props and "housenumber" in props:
            name["primary"] = props["street"] + " " + props["housenumber"]
        else:
            name["primary"] = "Miesto bez názvu"

        if "city" in props:
            name["secondary"] = props["city"]
            if "locality" in props:
                name["secondary"] += f", {props['locality']}"

        return name

    @staticmethod
    def get_type_from_geojson(obj: dict) -> PlaceType:
        props = obj["properties"]

        if "osm_key" in props:
            if props["osm_key"] == "highway":
                return PlaceType.STREET

            if props["osm_key"] == "shop":
                return PlaceType.SHOP

            if props["osm_key"] == "amenity":
                return PlaceType.INSTITUTION

        if "osm_value" in props:
            if (
                props["osm_value"] == "bus_stop"
                or props["osm_value"] == "platform"
            ):
                return PlaceType.STOP

            if props["osm_value"] in [
                "town",
                "neighbourhood",
                "suburb",
                "village",
            ]:
                return PlaceType.TOWN

            if props["osm_value"] in ["residential"]:
                return PlaceType.BUILDING

            if props["osm_value"] in ["school"]:
                return PlaceType.INSTITUTION

        if "street" in props and "housenumber" in props:
            return PlaceType.BUILDING

        return PlaceType.OTHER