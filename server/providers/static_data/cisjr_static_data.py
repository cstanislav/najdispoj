import logging
import os
import requests

from server.interfaces.i_static_data_provider import IStaticDataProvider
from server.models.gtfs_folder import GTFSFolder
from server.models.osm_file import OSMFile

from server.credentials import leoexpress_url


class CISJRStaticDataProvider(IStaticDataProvider):
    """
    Provider of raw GTFS data of CIS JŘ, from data.jr.ggu.cz.
    Automated nightly conversion of publicly available data from CZPTT and JDF formats.
    """

    def __init__(self, path, label="CISJR"):
        self.folder = GTFSFolder(path).set_label(label)
        self.label = label

    def load_data(self, osm_file: OSMFile) -> GTFSFolder | None:
        """Download GTFS data to the given path."""
        gtfs_url = "https://data.jr.ggu.cz/results/latest/JDF_merged_GTFS.zip"
        zip_path = f"{self.folder.path}gtfs.zip"

        logging.info(f"🚉CIS JŘ Downloading GTFS data from {gtfs_url}...")
        try:
            os.makedirs(os.path.dirname(zip_path), exist_ok=True)
            with open(zip_path, "wb+") as file:
                # urllib.urlretrieve can't be used due to SSL verification
                response = requests.get(gtfs_url)
                file.write(response.content)
        except requests.exceptions.RequestException as e:
            logging.info(f"❌ Error: {e}")
            logging.info("❌ Download has failed.")
            return None

        logging.info("✅ Done.")

        return (
            self.folder
            .load_zip(zip_path)
            .remove_transfers() # Remove transfers.txt - contains invalid stop_ids
            # .fix_service_ids()
            # .replace_route_types("11", "3")
            # .generate_shapes(osm_file)
            # .revert_routes()
            # .replace_route_types("11", "800")
        )
