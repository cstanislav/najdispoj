import urllib.request
import urllib.error
import urllib.parse
import os
import logging

from server.interfaces.i_geodata_provider import IGeodataProvider
from server.models.osm_file import OSMFile


class GeofabrikGeodataProvider(IGeodataProvider):
    """Provider of raw OSM data from Geofabrik."""

    def __init__(self, path: str, url: str):
        self.path = path
        self.url = url

    def load_data(self) -> OSMFile | None:
        os.makedirs(os.path.dirname(self.path), exist_ok=True)

        try:
            logging.info(
                f"🗺️  Downloading raw OSM data from {self.url} to {self.path}..."
            )
            urllib.request.urlretrieve(self.url, self.path)
        except urllib.error.URLError:
            logging.error("❌ Download has failed.")
            return None

        logging.info("✅ Done.")
        return OSMFile(self.path)
