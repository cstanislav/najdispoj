import urllib.request
import urllib.error
import urllib.parse
import os
import logging

from server.interfaces.i_geodata_provider import IGeodataProvider
from server.models.osm_file import OSMFile
from server.models.bounding_box import BoundingBox


class OverpassAPIGeodataProvider(IGeodataProvider):
    """Provider of raw OSM data from Overpass API."""

    def __init__(self, path: str, bounding_box: BoundingBox, address: str = "https://overpass-api.de/api"):
        # IP address and port of an Overpass API instance (without the trailing '/').
        self.path = path
        self.bounding_box = bounding_box
        self.address = address


    def load_data(self) -> OSMFile | None:
        os.makedirs(os.path.dirname(self.path), exist_ok=True)

        url = self.address + "/map?bbox=" + str(self.bounding_box)
        temp_path = self.path + ".temp.osm"
        try:
            logging.info(
                f"🗺️  Downloading raw OSM data from {url} to {temp_path}..."
            )
            urllib.request.urlretrieve(url, temp_path)
        except urllib.error.URLError:
            logging.error("❌ Download has failed.")
            return None

        # Convert to pbf
        logging.info("🗺️  Converting to PBF...")
        OSMFile(temp_path).cat(OSMFile(self.path))
        os.remove(temp_path)

        logging.info("✅ Done.")
        return OSMFile(self.path)
