import json
from numpy import short
import requests
import logging
from enum import Enum

from server.models.coords import Coords
from server.models.get_plan_params import GetPlanParams
from server.models.transit_mode import TransitMode
from server.models.stop import Stop
from server.models.leg import Leg
from server.models.itinerary import Itinerary
from server.models.plan import Plan

from server.interfaces.i_routing_provider import IRoutingProvider

import server.config as config


class OpenTripPlanner2RoutingProvider(IRoutingProvider):
    """
    OpenTripPlanner routing provider.

    Args:
        address (str): IP address and port of an OpenTripPlanner instance
            (without the trailing /)
    """

    def __init__(
        self,
        address,
    ):
        self.address = address

    @staticmethod
    def prepare_query(filename: str, parameters: dict):
        """Reads a GraphQL query from a file and replaces placeholders with parameters."""
        with open(filename, "r") as file:
            query = file.read()

        for key, value in parameters.items():
            query = query.replace(f"${key}", str(value))

        return query

    def execute_query(self, query: str):
        """Execute a GraphQL query."""
        query = query.replace("\n", "\\n")  # Escape newlines
        query = query.replace('"', '\\"')  # Escape double quotes

        data = f'{{"query": "{query}"}}'
        response = requests.post(
            f"{self.address}/otp/routers/default/index/graphql",
            data=data,
            headers={"Content-Type": "application/json"},  # Would return 415 otherwise
        )
        logging.info(f"OpenTripPlanner POST url: {response.url}")
        logging.info(f"Data: {data}")

        return response

    def get_plan(self, params: GetPlanParams) -> Plan:
        walkSpeedOptions = [0.8, 1.3, 2.22, 3.5]  # meters/second

        parameters: dict = {
            "from_lat": params["origin"].lat,
            "from_lon": params["origin"].lng,
            "to_lat": params["destination"].lat,
            "to_lon": params["destination"].lng,
            "date": params["date"],
            "time": params["time"],
            "numItineraries": 5,
            "arriveBy": "true" if params["arrive_by"] else "false",
            "minTransferTime": 120,
            "maxTransfers": params["max_transfers"],
            "walkReluctance": params["walk_reluctance"],
            "walkSpeed": walkSpeedOptions[params["walk_speed"]],
            "mode": "TRANSIT,WALK",
            "boardSlack": 0,
            "alightSlack": 0,
        }

        # Get data from OTP
        query = self.prepare_query(
            "server/queries/opentripplanner2_plan.graphql", parameters
        )
        logging.info(f"Prepared query: {query}")
        data = self.execute_query(query)

        logging.info(f"Querying OpenTripPlanner at url {data.url}")
        logging.info(f"Response content {data.content}")
        data = json.loads(data.content)

        logging.info(f"Data: {data}")

        plan = self._create_plan(data["data"])

        if config.debug_mode:
            plan.set_raw(data)

        return plan

    def _create_plan(self, data):
        if ("plan" not in data) or ("itineraries" not in data["plan"]):
            logging.info("No plan found.")
            return Plan()

        itineraries = []
        for itinerary in data["plan"]["itineraries"]:
            logging.info(f"Creating itinerary {itinerary}")
            itineraries.append(self._create_itinerary(itinerary))

        plan = Plan(itineraries)
        plan.compute_timelines()

        return plan

    def _create_itinerary(self, itinerary):
        legs = []
        for index, leg in enumerate(itinerary["legs"]):
            last = len(legs) - 1 == index
            legs.append(self._create_leg(leg, last))

        return Itinerary(
            itinerary["duration"],
            itinerary["startTime"],
            itinerary["endTime"],
            legs,
        )

    def _create_leg(self, leg, last):
        def leg_mode(route_type: int | None) -> TransitMode:
            mode_by_route_type = {
                0: TransitMode.TRAM,
                1: TransitMode.SUBWAY,
                2: TransitMode.RAIL,
                3: TransitMode.BUS,
                4: TransitMode.FERRY,
                11: TransitMode.TROLLEYBUS,
                800: TransitMode.TROLLEYBUS,
            }

            if route_type == None:
                return TransitMode.WALK

            if route_type in mode_by_route_type:
                return mode_by_route_type[route_type]

            return TransitMode.UNKNOWN

        if last:
            duration = leg["duration"] - 60
        else:
            duration = leg["duration"]

        stops = []
        stops.append(self._create_stop(leg["from"]))
        if leg["intermediatePlaces"] != None:
            for stop in leg["intermediatePlaces"]:
                stops.append(self._create_stop(stop))
        stops.append(self._create_stop(leg["to"], True))

        number = leg["route"]["shortName"] if leg["route"] else ""
        short_name = ""
        # short_name = leg["tripShortName"] if "tripShortName" in leg else "" ??????????
        route_type = leg["route"]["type"] if leg["route"] else None

        headsign = leg["headsign"]
        agency_id = leg["agency"]["gtfsId"].split(":")[0] if leg["agency"] else None

        return Leg(
            leg_mode(route_type),
            duration,
            leg["distance"],
            leg["legGeometry"]["points"],
            leg["startTime"],
            leg["to"]["arrivalTime"],
            number,
            short_name,
            headsign,
            stops,
            agency_id,
        )

    def _create_stop(self, stop, last=False):
        def stop_name(n):
            if n == "Origin" or n == "Destination":
                return ""
            return n

        return Stop(
            stop_name(stop["name"]),
            Coords(stop["lat"], stop["lon"]),
            stop["arrivalTime"] if last else stop["departureTime"],
        )
