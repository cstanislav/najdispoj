import json
import requests
import logging
from warnings import deprecated

from server.models.coords import Coords
from server.models.get_plan_params import GetPlanParams
from server.models.transit_mode import TransitMode
from server.models.stop import Stop
from server.models.leg import Leg
from server.models.itinerary import Itinerary
from server.models.plan import Plan

from server.interfaces.i_routing_provider import IRoutingProvider

import server.config as config

@deprecated
class OpenTripPlannerRoutingProvider(IRoutingProvider):
    def __init__(self, address, region: str = "default", is_v2: bool = False):
        # IP address and port of an OpenTripPlanner 1.5.0 instance
        # (Without the trailing /)
        self.address = address
        self.region = region
        self.is_v2 = is_v2

    def _create_stop(self, stop, last=False):
        def stop_name(n):
            if n == "Origin" or n == "Destination":
                return ""
            return n

        return Stop(
            stop_name(stop["name"]),
            Coords(stop["lat"], stop["lon"]),  # TODO maybe convert to float
            stop["arrival"] if last else stop["departure"],
        )

    def _leg_mode(self, route_type: int | None) -> TransitMode:
        mode_by_route_type = {
            0: TransitMode.TRAM,
            1: TransitMode.SUBWAY,
            2: TransitMode.RAIL,
            3: TransitMode.BUS,
            4: TransitMode.FERRY,
            11: TransitMode.TROLLEYBUS,
            800: TransitMode.TROLLEYBUS,
        }

        if route_type == None:
            return TransitMode.WALK

        if route_type in mode_by_route_type:
            return mode_by_route_type[route_type]

        return TransitMode.UNKNOWN

    def _create_leg(self, leg, last):
        if last:
            duration = leg["duration"] - 60
        else:
            duration = leg["duration"]

        stops = []
        stops.append(self._create_stop(leg["from"]))
        if "intermediateStops" in leg:
            for stop in leg["intermediateStops"]:
                stops.append(self._create_stop(stop))
        stops.append(self._create_stop(leg["to"], True))

        number = ""
        if "routeShortName" in leg:
            number = leg["routeShortName"]

        short_name = leg["tripShortName"] if "tripShortName" in leg else ""
        headsign = leg["headsign"] if "headsign" in leg else ""

        route_type = leg["routeType"] if "routeType" in leg else None

        return Leg(
            self._leg_mode(route_type),
            duration,
            leg["distance"],
            leg["legGeometry"]["points"],
            leg["startTime"],
            leg["to"]["arrival"],
            number,
            short_name,
            headsign,
            stops,
            leg["agencyId"] if "agencyId" in leg else None,
        )

    def _create_itinerary(self, itinerary):
        legs = []
        for index, leg in enumerate(itinerary["legs"]):
            last = len(legs) - 1 == index
            legs.append(self._create_leg(leg, last))

        return Itinerary(
            itinerary["duration"],
            itinerary["startTime"],
            itinerary["endTime"],
            legs,
        )

    def _create_plan(self, data):
        if ("plan" not in data) or ("itineraries" not in data["plan"]):
            return Plan()

        itineraries = []
        for itinerary in data["plan"]["itineraries"]:
            itineraries.append(self._create_itinerary(itinerary))

        return Plan(itineraries)

    def get_plan(self, params: GetPlanParams) -> Plan:
        # TODO specify a class (interface) for params
        walkSpeedOptions = [0.8, 1.3, 2.22, 3.5]  # meters/second
        optimizeOptions = {"quick": "QUICK", "transfers": "TRANSFERS"}

        # TODO include these parameters in query:
        # wheelchair, locale

        parameters: dict = {
            "fromPlace": str(params["origin"]),
            "toPlace": str(params["destination"]),
            "time": params["time"],
            "date": params["date"],
            "showIntermediateStops": True,
            "arriveBy": params["arrive_by"],
            "numItineraries": 5,
            "minTransferTime": 120,
            # "maxTransfers": params["max_transfers"],
            "walkSpeed": walkSpeedOptions[params["walk_speed"]],
            "mode": "TRANSIT,WALK",
            "transferSlack": 0,
            "boardSlack": 0,
            "alightSlack": 0,
        }
        if not self.is_v2:
            parameters["optimize"] = (optimizeOptions[params["optimize"]],)

        # Get data from OTP
        data = requests.get(
            f"{self.address}/otp/routers/{self.region}/plan?",
            params=parameters,
        )
        logging.info(f"OpenTripPlanner GET url: {data.url}")
        logging.debug(data.content)
        data = json.loads(data.content)

        plan = self._create_plan(data)
        plan.compute_timelines()  # TODO maybe move to _create_plan()

        if config.debug_mode:
            plan.set_raw(data)

        return plan
