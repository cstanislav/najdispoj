import logging
import collections

from server.interfaces.i_geocoding_provider import IGeocodingProvider
from server.models.coords import Coords
from server.models.place import Place, PlaceName, PlaceType
from server.services.geodata_service import GeodataService


class GeocodingService:
    def __init__(
        self,
        geodata_service: GeodataService
    ):
        self.geodata_service = geodata_service
        self.providers: dict[str, IGeocodingProvider] = \
            collections.OrderedDict()

    def register_provider(self, name: str, provider: IGeocodingProvider):
        """Registers and initializes a geocoding provider."""
        logging.info("➕ Registered a geocoding provider: %s", name)
        self.providers[name] = provider
        return self

    def autocomplete(self, query: str) -> list[Place]:
        """Returns places that match the query string."""
        result = []
        for provider in self.providers.values():
            try:
                result += provider.autocomplete(query)
            except NotImplementedError:
                pass

        Place.sort_places(result)

        return result

    def reverse_geocode(self, coords: Coords) -> Place:
        """Returns the name of a place based on its coordinates."""
        for provider in self.providers.values():
            try:
                return provider.reverse_geocode(coords)
            except NotImplementedError:
                pass
        return Place({"primary": "Unknown", "secondary": ""},
                     coords,
                     PlaceType.OTHER)
