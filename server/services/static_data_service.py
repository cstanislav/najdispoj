import logging

from server.interfaces.i_static_data_provider import IStaticDataProvider
from server.models.gtfs_folder import GTFSFolder
from server.services.geodata_service import GeodataService


class StaticDataService:
    """
    TODO differentiate between downloading and "applying" (setting as currently
    used) GTFS data. Add config parameter "preserve old datasets".
    """

    def __init__(self, geodata_service: GeodataService):
        self.geodata_service = geodata_service
        self.providers: dict[str, IStaticDataProvider] = {}

    def register_provider(self, name: str, provider: IStaticDataProvider):
        """Registers and initializes a static data provider."""
        logging.info("➕🚉 Registered a static data provider: %s", name)
        self.providers[name] = provider

    def get_provider(self, name: str) -> IStaticDataProvider:
        return self.providers[name]

    def load_data(self, providers: list[str] = []) -> list[GTFSFolder]:
        """Downloads GTFS data for all providers or for the specified ones. 
        Returns list of successfully downloaded GTFS folders."""
        gtfs_folders = []
        if not providers:
            providers = list(self.providers.keys())

        for provider in providers:
            gtfs_folder = self.providers[provider].load_data(self.geodata_service.merged_filtered_osm)
            if not gtfs_folder:
                logging.error(f"Failed to download GTFS data for {provider}.")
                continue
            gtfs_folders.append(gtfs_folder)
        
        return gtfs_folders
