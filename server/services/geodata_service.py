import logging

from server.models.osm_file import OSMFile
from server.interfaces.i_geodata_provider import IGeodataProvider

import server.config as cfg


class GeodataService:
    def __init__(
        self,
        merged_osm: OSMFile = OSMFile("/mnt/data/osm/merged.osm"),
        merged_filtered_osm: OSMFile = OSMFile("/mnt/data/osm/merged_filtered.osm"),
        merged_filtered_pbf: OSMFile = OSMFile("/mnt/data/osm/merged_filtered.osm.pbf"),
    ):
        self.merged_osm = merged_osm
        self.merged_filtered_osm = merged_filtered_osm
        self.merged_filtered_pbf = merged_filtered_pbf

        self.providers: dict[str, IGeodataProvider] = {}

    def register_provider(self, name: str, provider: IGeodataProvider):
        """Registers and initializes a geodata provider."""
        logging.info("➕ Registered a geodata provider: %s", name)
        self.providers[name] = provider
        return self

    def load_data(self, providers: list[str] = []) -> list[OSMFile]:
        """Downloads geodata for all providers or for the specified ones.
        Returns list of successfully downloaded OSM files."""
        osm_files = []
        if not providers:
            providers = list(self.providers.keys())

        for provider in providers:
            osm_file = self.providers[provider].load_data()
            if not osm_file:
                logging.error(f"Failed to download geodata for {provider}.")
                continue
            osm_files.append(osm_file)
        
        # Merge OSM files
        OSMFile.merge(osm_files, self.merged_osm)

        # Filter merged OSM data
        self.merged_osm.filter(self.merged_filtered_osm)
        
        # Convert filtered OSM data to PBF
        self.merged_filtered_osm.cat(self.merged_filtered_pbf)

        return osm_files
