import unittest
from unittest.mock import MagicMock
from server.services.routing_service import RoutingService

class TestRoutingService(unittest.TestCase):
    def setUp(self):
        self.static_data_service_mock = MagicMock()
        self.geodata_service_mock = MagicMock()
        self.routing_service = RoutingService(
            static_data_service=self.static_data_service_mock,
            geodata_service=self.geodata_service_mock
        )

    def test_register_provider(self):
        provider_name = "test_provider"
        provider_mock = MagicMock()
        self.routing_service.register_provider(provider_name, provider_mock)
        self.assertIn(provider_name, self.routing_service.providers)
        self.assertEqual(self.routing_service.providers[provider_name], provider_mock)

if __name__ == "__main__":
    unittest.main()