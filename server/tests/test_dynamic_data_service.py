import unittest
from unittest.mock import MagicMock

from server.services.dynamic_data_service import DynamicDataService


class MockProvider:
    pass


class TestDynamicDataService(unittest.TestCase):
    def setUp(self):
        self.dynamic_data_service = DynamicDataService()

    def test_register_provider(self):
        provider_name = "test_provider"
        provider_mock = MagicMock()
        self.dynamic_data_service.register_provider(provider_name, provider_mock)
        self.assertIn(provider_name, self.dynamic_data_service.providers)
        self.assertEqual(self.dynamic_data_service.providers[provider_name], provider_mock)

    def test_get_vehicles(self):
        provider_name = "test_provider"
        provider_mock = MagicMock()
        self.dynamic_data_service.providers[provider_name] = provider_mock
        self.dynamic_data_service.get_vehicles()
        provider_mock.get_vehicles.assert_called_once()