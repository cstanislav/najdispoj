import unittest
from unittest.mock import MagicMock
from server.services.static_data_service import StaticDataService


class MockProvider:
    pass

class TestStaticDataService(unittest.TestCase):
    def setUp(self):
        self.geodata_service_mock = MagicMock()
        self.static_data_service = StaticDataService(geodata_service=self.geodata_service_mock)

    def test_register_provider(self):
        provider_name = "test_provider"
        provider_mock = MagicMock()
        self.static_data_service.register_provider(provider_name, provider_mock)
        self.assertIn(provider_name, self.static_data_service.providers)
        self.assertEqual(self.static_data_service.providers[provider_name], provider_mock)

    def test_get_provider(self):
        provider_name = "test_provider"
        provider_mock = MagicMock()
        self.static_data_service.providers[provider_name] = provider_mock
        self.assertEqual(self.static_data_service.get_provider(provider_name), provider_mock)
