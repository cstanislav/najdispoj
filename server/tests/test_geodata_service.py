import unittest
from unittest.mock import MagicMock
from server.services.geodata_service import GeodataService

class TestGeodataService(unittest.TestCase):
    def setUp(self):
        self.geodata_service = GeodataService()

    def test_register_provider(self):
        provider_name = "test_provider"
        provider_mock = MagicMock()
        self.geodata_service.register_provider(provider_name, provider_mock)
        self.assertIn(provider_name, self.geodata_service.providers)
        self.assertEqual(self.geodata_service.providers[provider_name], provider_mock)

if __name__ == "__main__":
    unittest.main()