import unittest

from server.models.plan import Plan
from server.models.itinerary import Itinerary
from server.models.leg import Leg
from server.models.coords import Coords
from server.models.place import Place, PlaceType
from server.models.transit_mode import TransitMode
from server.models.stop import Stop

class TestPlan(unittest.TestCase):
    maxDiff = None

    def test_to_json(self):
        stop1 = Stop("Stop 1", Coords(5, 6), 10)
        stop2 = Stop("Stop 2", Coords(7, 8), 20)
        leg = Leg(
            TransitMode.BUS,
            10,
            20,
            "aaabbb",
            1,
            11,
            "Bus 123",
            "123",
            "Terminal 1",
            [stop1, stop2],
            "Transport Agency"
        )
        itinerary = Itinerary(10, 1, 11, [leg])
        plan = Plan([itinerary])

        self.assertEqual(plan.to_json(), {'itineraries': [{'duration': 10, 'departure': 1, 'arrival': 11, 'legs': [{'mode': 'BUS', 'duration': 10, 'distance': 20, 'path': 'aaabbb', 'departure': 1, 'arrival': 11, 'number': 'Bus 123', 'shortName': '123', 'headsign': 'Terminal 1', 'stops': [{'name': 'Stop 1', 'coords': {'lat': 5, 'lng': 6}, 'departure': 10}, {'name': 'Stop 2', 'coords': {'lat': 7, 'lng': 8}, 'departure': 20}], 'agencyId': 'Transport Agency', 'timelineDuration': None, 'waitingDuration': 0, 'totalDuration': 10}], 'timelineDuration': None}]})


if __name__ == "__main__":
    unittest.main()