import unittest
from unittest.mock import MagicMock
from server.services.api_service import APIService

class MockProvider:
    pass

class TestAPIService(unittest.TestCase):
    def setUp(self):
        self.geocoding_service_mock = MagicMock()
        self.routing_service_mock = MagicMock()
        self.dynamic_data_service_mock = MagicMock()
        self.api_service = APIService(
            geocoding_service=self.geocoding_service_mock,
            routing_service=self.routing_service_mock,
            dynamic_data_service=self.dynamic_data_service_mock
        )

    def test_reverse_geocode(self):
        coords = '{"lat": 49.195060, "lng": 16.606837}'
        self.api_service.reverse_geocode(coords)
        self.geocoding_service_mock.reverse_geocode.assert_called_once()

    def test_autocomplete(self):
        query = "Brno"
        self.api_service.autocomplete(query)
        self.geocoding_service_mock.autocomplete.assert_called_once()

    def test_get_plan(self):
        origin = '{"lat": 49.195060, "lng": 16.606837}'
        destination = '{"lat": 49.195060, "lng": 16.606837}'
        arrive_by = False
        date = "2022-01-01"
        time = "12:00:00"
        max_transfers = 3
        walk_reluctance = 3
        walk_speed = 3
        optimize = "QUICK"
        self.api_service.get_plan(
            origin=origin,
            destination=destination,
            arrive_by=arrive_by,
            date=date,
            time=time,
            max_transfers=max_transfers,
            walk_reluctance=walk_reluctance,
            walk_speed=walk_speed,
            optimize=optimize
        )
        self.routing_service_mock.get_plan.assert_called_once()
