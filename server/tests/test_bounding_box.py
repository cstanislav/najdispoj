import unittest

from server.models.bounding_box import BoundingBox
from server.models.coords import Coords

class TestBoundingBox(unittest.TestCase):
    def test_str(self):
        coords_min = Coords(1, 2)
        coords_max = Coords(3, 4)
        bounding_box = BoundingBox(coords_min, coords_max)
        self.assertEqual(str(bounding_box), "2,1,4,3")

    def test_add_coords(self):
        coords_min = Coords(1, 2)
        coords_max = Coords(3, 4)
        bounding_box = BoundingBox(coords_min, coords_max)
        coords = Coords(5, 6)
        bounding_box.add_coords(coords)
        self.assertEqual(str(bounding_box), "2,1,6,5")

        coords = Coords(0, 1)
        bounding_box.add_coords(coords)
        self.assertEqual(str(bounding_box), "1,0,6,5")

        coords = Coords(6, 7)
        bounding_box.add_coords(coords)
        self.assertEqual(str(bounding_box), "1,0,7,6")

        coords = Coords(4, 5)
        bounding_box.add_coords(coords)
        self.assertEqual(str(bounding_box), "1,0,7,6")