from fastapi import FastAPI
import logging

from server.services.geodata_service import GeodataService
from server.services.geocoding_service import GeocodingService
from server.services.routing_service import RoutingService
from server.services.static_data_service import StaticDataService
from server.services.dynamic_data_service import DynamicDataService
from server.services.api_service import APIService

# Clear logging handlers - without this line the basicConfig would sometimes be ignored
logging.root.handlers = ([])  
logging.basicConfig(
    level=logging.INFO,
    format="%(asctime)s.%(msecs)03d - %(levelname)s - %(message)s",
    datefmt="%Y-%m-%d %H:%M:%S",
)

geodata_service = GeodataService()
geocoding_service = GeocodingService(geodata_service)
static_data_service = StaticDataService(geodata_service)
routing_service = RoutingService(static_data_service, geodata_service)
dynamic_data_service = DynamicDataService()
api_service = APIService(geocoding_service, routing_service, dynamic_data_service)

app = FastAPI()


@app.get("/api/reverse")
async def reverse(coords: str):
    return api_service.reverse_geocode(coords)


@app.get("/api/autocomplete")
async def autocomplete(query: str):
    return api_service.autocomplete(query)


@app.get("/api/plan")
async def plan(
    origin: str,
    destination: str,
    arriveBy: bool,
    date: str,
    time: str,
    maxTransfers: int,
    walkReluctance: int,
    walkSpeed: int,
    optimize: str,
):
    return api_service.get_plan(
        origin,
        destination,
        arriveBy,
        date,
        time,
        maxTransfers,
        walkReluctance,
        walkSpeed,
        optimize,
    )


@app.get("/api/vehicles")
async def vehicles():
    return api_service.get_vehicles()
