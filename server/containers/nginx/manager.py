import docker
import logging
import os
import shutil

from server.models.container import Container


class NginxManager:
    def __init__(
        self,
        host: str,
        port: int,
        najdispoj_port: int,
        dynamic_data_port: int = 0,
        restart_on_reload: bool = False,
    ):
        """
        Creates and runs the Nginx container.

        Args:
            host (str): The address where Najdispoj Server will be accessible,
                without the port number. E.g. api.example.org
            port (int): The port on which Najdispoj Server will be accessible from client.
            najdispoj_port (int): Nginx -> Najdispoj communication port.
            dynamic_data_port (int): The port on which to listen to dynamic
                data. 0 = No dynamic data
            restart_on_reload (bool): Whether to restart the container on reload.
        """
        self.host = host
        self.port = port
        self.najdispoj_port = najdispoj_port
        self.dynamic_data_port = dynamic_data_port
        self.restart_on_reload = restart_on_reload
        self.client = docker.from_env()
        self.container = self.create_container()

    def build_client(self, client_root="/usr/src/app/client"):
        """Builds the client webapp and copies it to client volume."""
        logging.info("Building client...")

        current_dir = os.getcwd()

        os.chdir(client_root)
        os.system("npm install")
        logging.info("📦 Installed npm dependencies. Running doctor:")
        os.system("npm doctor")
        logging.info("📦 Installed npm dependencies.")
        os.system("npm run generate")
        os.chdir(current_dir)

        logging.info("✅ Successfully built client.")

        self.copy_client(client_root)

    def copy_client(self, client_root):
        logging.info('Copying client to volume "client"...')

        source = client_root + r"/.output/public/"
        destination = r"/mnt/client/"

        os.system("rm -r " + destination + "*")  # Remove contents of the folder
        os.system("rm -r " + destination + ".*")  # Remove hidden files

        # Copy new files into the folder
        files = os.listdir(source)
        for f in files:
            shutil.move(source + f, destination + f)

    def create_container(self):
        ports = {
            "80": 80,  # Used for http -> https redirect
            self.port: self.port,
        }
        if self.dynamic_data_port != 0:  # Used by DPB dynamic data provider
            ports[str(self.dynamic_data_port) + "/udp"] = self.dynamic_data_port

        return Container(
            self.client,
            os.path.dirname(__file__),
            "najdispoj-nginx:latest",  # Tag: image name
            "najdispoj-nginx",  # Name: container name
            "najdispoj-network",  # Declared in docker-compose.yml
            ports,
            {
                "najdispoj_client": {"bind": "/mnt/client", "mode": "ro"},
                "/etc/letsencrypt": {"bind": "/etc/letsencrypt", "mode": "ro"},
            },
            buildargs={
                "HOST": self.host,
                "PORT": str(self.port),
                "NAJDISPOJ_PORT": str(self.najdispoj_port),
                "DYNAMIC_DATA_PORT": str(self.dynamic_data_port),
            },
            restart_on_reload=self.restart_on_reload,
        )
