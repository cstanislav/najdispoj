import docker
from docker.client import DockerClient
from docker.models.resource import Model
from docker.types import CancellableStream

import logging
import os

from server.models.gtfs_folder import GTFSFolder

import server.config as config
from server.services.routing_service import RoutingService

VOLUMES = ["najdispoj_otp2:/var/opentripplanner", "najdispoj_data:/mnt/data"]
ENVIRONMENT = ["JAVA_TOOL_OPTIONS='-Xmx18G'"]
IMAGE_NAME = "najdispoj-otp2"
CONTAINER_SERVE_NAME = "najdispoj-otp2-serve"
CONTAINER_BUILD_NAME = "najdispoj-otp2-build"


class OpenTripPlanner2Manager:
    """
    This class can be fully rewritten to accommodate the needs
    of your specific Najdispoj instance. The functions should then be called
    from main.py according to your needs.
    """

    def __init__(self, routing_service: RoutingService, external_port=25515):
        """
        Initializes the OpenTripPlanner 2 manager.

        Args:
            external_port (int, optional): The port on which the service will be accessible outside najdispoj-network
                (e.g. for debugging purposes)
        """
        logging.info(f"OTP2 - Initializing manager. External port: {external_port}.")

        self.routing_service = routing_service
        self.external_port = external_port
        self.docker: DockerClient = docker.from_env()

        result = self.docker.images.build(
            path=os.path.dirname(__file__),
            nocache=True,
            pull=True,
            tag=IMAGE_NAME,
            rm=True,  # Removes intermediate containers - saves space
        )

        # Create the OTP2 directory
        os.makedirs("/mnt/otp2", exist_ok=True)

        # Copy files from server/containers/opentripplanner2/config to /mnt/otp2
        os.system("cp -r server/containers/opentripplanner2/config/* /mnt/otp2")

        if result and type(result) == tuple and len(result) > 0:
            self.image: Model = result[0]
        else:
            raise Exception("No image was built.")

    def get_instance_address(self):
        return f"http://{CONTAINER_SERVE_NAME}:8080"

    def build_street_graph(self):
        """Runs OpenTripPlanner 2 with --buildStreet --save options."""
        logging.info("  OTP2 - Building street graph...")

        pbf_file = "/mnt/otp2/geodata.osm.pbf"

        os.system(f"rm -rf {pbf_file}")

        pbf_path = self.routing_service.geodata_service.merged_filtered_pbf.path

        # Create a symlink of the PBF file in OTP directory
        os.symlink(pbf_path, pbf_file)

        container = self.docker.containers.run(  # Run OTP2
            self.image.tags[0],
            name=CONTAINER_BUILD_NAME,
            volumes=VOLUMES,
            environment=ENVIRONMENT,
            command="docker-entrypoint.sh --buildStreet --save",
            detach=True,
            auto_remove=True,
            labels={"com.docker.compose.project": "najdispoj"},
        )

        log_generator = container.logs(
            stream=True, timestamps=True, follow=True
        )  # Log output

        success = False
        try:
            while True:
                line = next(log_generator).decode("utf-8").strip("\r\n")
                logging.info(f"OTP2 - {line}")
                if "Done building graph. Exiting." in line:
                    success = True
        except StopIteration:
            logging.info(f"End of log stream.")

        if not success:
            logging.error("OTP2 - ❌ Failed to build street graph.")
        else:
            logging.info("OTP2 - ✅ Street graph built.")

        container.wait()  # Wait until the container is stopped

        return self

    def build_graph(self, gtfs_folders: list[GTFSFolder]):
        """Runs OpenTripPlanner 2 with --loadStreet --save options."""
        logging.info("  OTP2 - Building graph...")

        # Clear the OTP2 directory of previous GTFS files
        os.system("rm -rf /mnt/otp2/gtfs_*.zip")

        for gtfs_folder in gtfs_folders:  # Zip and move GTFS files to OTP directory
            gtfs_folder.zip(f"/mnt/otp2/gtfs_{gtfs_folder.label}.zip")

        container = self.docker.containers.run(  # Run OTP2
            self.image.tags[0],
            name=CONTAINER_BUILD_NAME,
            volumes=VOLUMES,
            environment=ENVIRONMENT,
            command="docker-entrypoint.sh --loadStreet --save",
            detach=True,
            auto_remove=True,
            labels={"com.docker.compose.project": "najdispoj"},
        )

        log_generator = container.logs(
            stream=True, timestamps=True, follow=True
        )  # Log output

        success = False
        while True:
            try:
                line = next(log_generator).decode("utf-8").strip("\r\n")
                logging.info(f"OTP2 - {line}")
                if "Done building graph. Exiting." in line:
                    success = True
            except StopIteration:
                break

        if not success:
            logging.error("OTP2 - ❌ Failed to build graph.")
        else:
            logging.info("OTP2 - ✅ Graph built.")

        container.wait()  # Wait until the container is stopped

        return self

    def serve(self, restart=True):
        """Runs OpenTripPlanner 2 with --load option."""

        def exists():
            """Returns True if OTP2 is running."""
            return (
                len(self.docker.containers.list(filters={"name": CONTAINER_SERVE_NAME}))
                > 0
            )

        logging.info(
            f"OTP2 - Serving data. Graphiql url: http://localhost:{self.external_port}/graphiql"
        )

        if exists():
            if restart:
                self.docker.containers.get(CONTAINER_SERVE_NAME).kill()
                logging.info("OTP2 - Killed existing container (restart=True).")
            else:
                logging.info("OTP2 - Already running.")
                return self

        logging.info("OTP2 - Starting container...")
        container = self.docker.containers.run(  # Run OTP2
            self.image.tags[0],
            name=CONTAINER_SERVE_NAME,
            volumes=VOLUMES,
            environment=ENVIRONMENT,
            network="najdispoj-network",
            command="docker-entrypoint.sh --load",
            detach=True,
            auto_remove=True,
            ports={f"8080/tcp": self.external_port},
            labels={"com.docker.compose.project": "najdispoj"},
        )

        log_generator = container.logs(stream=True, follow=True)  # Log output

        try:
            while True:
                line = next(log_generator).decode("utf-8").strip("\r\n")
                logging.info(f"OTP2 - {line}")

                if "server running" in line:
                    raise StopIteration
        except StopIteration:
            logging.info(f"OTP2 - End of log stream.")

        logging.info("OTP2 - Data are being served.")

        return self
