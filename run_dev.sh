# Remove all containers starting with "najdispoj"
docker ps -a | grep najdispoj | awk '{print $1}' | xargs docker rm -f

docker compose build --progress=plain
docker compose up --remove-orphans -d
docker logs najdispoj-server -f
