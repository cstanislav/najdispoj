import skLocale from "@/locales/sk";
import csLocale from "@/locales/cs";
import enLocale from "@/locales/en";
import config from "@/config";

function skPlural(number: number) {
  if (number == 0) {
    return 0;
  } else if (number == 1) {
    return 1;
  } else if (number > 1 && number < 5) {
    return 2;
  } else {
    return 3;
  }
}

export default defineI18nConfig(() => ({
  legacy: false,
  locale: config.locales.default,
  fallbackLocale: config.locales.fallback,
  pluralRules: {
    sk: skPlural,
    cs: skPlural,
  },
  messages: {
    sk: skLocale,
    cs: csLocale,
    en: enLocale,
  },
}));
