const sw = process.env.SW === 'true'

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  css: ["~/assets/scss/main.scss"],
  devtools: { enabled: false },
  ssr: false,
  modules: [
    "@pinia/nuxt",
    '@pinia-plugin-persistedstate/nuxt',
    ["@nuxtjs/i18n", { vueI18n: "@/i18n.config.ts" }],
    "@nuxt/ui",
    '@vite-pwa/nuxt',
  ],
  vite: {
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `
            @use "sass:math";
            @import "@/assets/scss/variables.scss";
          `,
        },
      },
    },
  },
  postcss: {
    plugins: {
      tailwindcss: {},
      autoprefixer: {},
    },
  },
  app: {
    head: {
      link: [
        { rel: "icon", type: "image/svg", href: "/favicon.svg" }
      ],
      htmlAttrs: {
        prefix: "og: http://ogp.me/ns#",
      },
      meta: [
        { charset: "utf-8" },
        {
          name: "viewport",
          content: "width=device-width, initial-scale=1, shrink-to-fit=no",
        },
        { name: "theme-color", content: "#ffffff" },
        { name: "description", content: "Najdispoj Trip Planner" },
        { property: "og:locale", content: "sk_SK" },
        { property: "og:type", content: "website" },
        { property: "og:title", content: "Najdispoj" },
        { property: "og:description", content: "Najdispoj Trip Planner" },
        { property: "og:url", content: "https://najdispoj.sk" },
        { property: "og:site_name", content: "Najdispoj" },
        { property: "og:image", content: "./ogimage.png" },
      ],
    },
  },
  pwa: {
    strategies: sw ? 'injectManifest' : 'generateSW',
    srcDir: sw ? 'service-worker' : undefined,
    filename: sw ? 'sw.ts' : undefined,
    registerType: 'autoUpdate',
    manifest: {
      "name": "Najdispoj",
      "short_name": "Najdispoj",
      "theme_color": "#ffffff",
      "background_color": "#f50101",
      "display": "standalone",
      // "start_url": "http://localhost:8080",
      "scope": "/",
      "description": "Najdispoj Trip Planner",
      "orientation": "natural",
      "icons": [
        {
          src: 'pwa-64x64.png',
          sizes: '64x64',
          type: 'image/png'
        },
        {
          src: 'pwa-192x192.png',
          sizes: '192x192',
          type: 'image/png'
        },
        {
          src: 'pwa-512x512.png',
          sizes: '512x512',
          type: 'image/png',
          purpose: 'any'  
        },
        {
          src: 'maskable-icon-512x512.png',
          sizes: '512x512',
          type: 'image/png',
          purpose: 'maskable'
        }
      ]
      // name: 'Najdispoj Trip Planner',
      // short_name: 'Najdispoj',
      // theme_color: '#ffffff',
      // icons: [
      //   {
      //     src: 'pwa-192x192.png',
      //     sizes: '192x192',
      //     type: 'image/png',
      //   },
      //   {
      //     src: 'pwa-512x512.png',
      //     sizes: '512x512',
      //     type: 'image/png',
      //   },
      //   {
      //     src: 'pwa-512x512.png',
      //     sizes: '512x512',
      //     type: 'image/png',
      //     purpose: 'any maskable',
      //   },
      // ],
    },
    client: {
      installPrompt: true,
    },
    devOptions: {
      enabled: true,
      suppressWarnings: true,
      navigateFallback: '/',
      navigateFallbackAllowlist: [/^\/$/],
      type: 'module',
    },
  }
});
