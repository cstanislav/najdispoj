const fetchResponse = async (request) => {
  // Next try to get the resource from the network
  try {
    return await fetch(request);
  } catch (error) {
    return new Response('Network error happened', {
      status: 408,
      headers: { 'Content-Type': 'text/plain' },
    });
  }
};

self.addEventListener('fetch', (event) => {
  event.respondWith(
    fetchResponse(event.request)
  );
});