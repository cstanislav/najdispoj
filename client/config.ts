/**
 * Default config - do not modify
 *
 * Put the modified file into /overrides/<override>/client/
 */

export default {
  title: "Najdispoj",
  ticket: false,
  api: {
    host: "http://localhost:8081/api",
  },
  locales: {
    default: "sk",
    fallback: "en",
    supported: [
      {
        code: "sk",
        name: "Slovenčina"
      },
      {
        code: "cs",
        name: "Čeština"
      },
      {
        code: "en",
        name: "English"
      },
    ],
  },
  map: {
    vehicles: true,
    vehiclesUrl: "",
    center: {
      lat: 48.1549,
      lng: 17.129043,
    },
    zoom: 14,
  },
  sources: [
    {
      name: "Dopravný podnik Bratislava, a.s.",
      url: "https://dpb.sk/",
    },
    {
      name: "Bratislavská integrovaná doprava, a.s.",
      url: "https://www.bid.sk/",
    },
    {
      name: "Dopravní podnik města Olomouce, a.s.",
      url: "https://www.dpmo.cz/",
    },
    {
      name: "Železnice Slovenskej republiky",
      url: "https://www.zsr.sk/",
    },
    {
      name: "KORDIS JMK, a.s.",
      url: "https://www.idsjmk.cz/",
    },
    {
      name: "Leo Express, s.r.o.",
      url: "https://www.leoexpress.com/",
    },
    {
      name: "Regionální organizátor pražské integrované dopravy, p. o.",
      url: "https://pid.cz/",
    },
    {
      name: "Dopravní podnik měst Liberce a Jablonce nad Nisou, a. s.",
      url: "https://www.dpmlj.cz/",
    },
    {
      name: "OpenStreetMap contributors",
      url: "https://www.openstreetmap.org/",
    },
  ],
};
