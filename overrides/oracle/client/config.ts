/**
 * Default config - do not modify
 *
 * Put the modified file into /overrides/<override>/client/
 */

export default {
  title: "Najdispoj",
  ticket: false,
  api: {
    host: "https://najdispoj.sk/api",
  },
  locales: {
    default: "sk",
    fallback: "en",
    supported: [
      {
        code: "sk",
        name: "Slovenčina"
      },
      {
        code: "cs",
        name: "Čeština"
      },
      {
        code: "en",
        name: "English"
      },
    ],
  },
  map: {
    vehicles: true,
    vehiclesUrl: "",
    center: {
      lat: 48.817,
      lng: 18.350,
    },
    zoom: 12,
  },
  sources: [
    {
      name: "Dopravný podnik Bratislava, a.s.",
      url: "https://dpb.sk/",
    },
    {
      name: "Bratislavská integrovaná doprava, a.s.",
      url: "https://www.bid.sk/",
    },
    {
      name: "Dopravní podnik města Olomouce, a.s.",
      url: "https://www.dpmo.cz/",
    },
    {
      name: "Železnice Slovenskej republiky",
      url: "https://www.zsr.sk/",
    },
    {
      name: "Leo Express, s.r.o.",
      url: "https://www.leoexpress.com/",
    },
    {
      name: "OpenStreetMap contributors",
      url: "https://www.openstreetmap.org/",
    },
  ],
};
