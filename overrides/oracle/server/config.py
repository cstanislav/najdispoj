"""
Default config - do not modify

Put the modified file into /overrides/<override>/server/

Provider-independent variables are set in this file.
Provider-dependent variables should be set in the provider's
constructor (in main.py).

This file should mostly contain information about the deployment environment.
"""

network = {
    "host": "najdispoj.sk",
    "port": 443,  # 443 for https, 80 for http
    # Nginx -> Najdispoj Server - also redefine in docker-compose.yml
    "najdispoj_port": 25516,
    "dynamic_data_port": 50801,
}

area = {
    "bbox": (
        {"lat": 49.724, "lng": 16.767},
        {"lat": 47.778, "lng": 22.547},
    ),
    "center": {"lat": 48.817, "lng": 18.350},
}

debug_mode = False
pfaedle_path = "/usr/src/app/vendor/pfaedle/build/pfaedle"
