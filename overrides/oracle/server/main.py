from apscheduler.schedulers.background import BackgroundScheduler

from server.init import (
    geodata_service,
    geocoding_service,
    static_data_service,
    routing_service,
    dynamic_data_service,
    app,
)
from server.providers.dynamic_data.otp2_dynamic_data import OTP2DynamicDataProvider

from server.providers.geodata.geofabrik import GeofabrikGeodataProvider
from server.providers.routing.opentripplanner2 import OpenTripPlanner2RoutingProvider
from server.providers.geocoding.photon import PhotonGeocodingProvider
from server.providers.agencies.dpmo.static_data import DPMOStaticDataProvider
from server.providers.agencies.dpb.static_data import DPBStaticDataProvider
from server.providers.agencies.dpb.dynamic_data import DPBDynamicDataProvider
from server.providers.agencies.ams.static_data import AMSStaticDataProvider
from server.providers.agencies.zsr.static_data import ZSRStaticDataProvider
from server.providers.agencies.leoexpress.static_data import LeoExpressStaticDataProvider
from server.containers.nginx.manager import NginxManager
from server.containers.opentripplanner2.manager import OpenTripPlanner2Manager
from server.credentials import dpb_host, dpb_port, dpb_login, dpb_pass
import server.config as config

geodata_service.register_provider(
    "Slovakia",
    GeofabrikGeodataProvider(
        "/mnt/data/osm/slovakia.osm.pbf",
        "https://download.geofabrik.de/europe/slovakia-latest.osm.pbf"
    )
)
geodata_service.register_provider(
    "Czech Republic",
    GeofabrikGeodataProvider(
        "/mnt/data/osm/czech_republic.osm.pbf",
        "https://download.geofabrik.de/europe/czech-republic-latest.osm.pbf"
    )
)

geocoding_service.register_provider("Photon", PhotonGeocodingProvider("https://photon.komoot.io"))

# static_data_service.register_provider("PID", PIDStaticDataProvider("/mnt/data/gtfs/pid/", "001_PID"))
# static_data_service.register_provider("DPMLJ", DPMLJStaticDataProvider("/mnt/data/gtfs/dpmlj/"))
static_data_service.register_provider("DPMO", DPMOStaticDataProvider("/mnt/data/gtfs/dpmo/"))
static_data_service.register_provider("ZSR", ZSRStaticDataProvider("/mnt/data/gtfs/zsr/"))
static_data_service.register_provider("DPB", DPBStaticDataProvider("/mnt/data/gtfs/dpb/", dpb_host, dpb_port, dpb_login, dpb_pass, "DPB"))
# static_data_service.register_provider("IDSJMK", IDSJMKStaticDataProvider("/mnt/data/gtfs/idsjmk/", "IDSJMK"))
static_data_service.register_provider("LeoExpress", LeoExpressStaticDataProvider("/mnt/data/gtfs/leoexpress/"))
static_data_service.register_provider("AMS", AMSStaticDataProvider("/mnt/data/gtfs/ams/"))

otp2_manager = OpenTripPlanner2Manager(routing_service)

routing_service.register_provider("OpenTripPlanner 2", OpenTripPlanner2RoutingProvider(otp2_manager.get_instance_address()))

nginx_manager = NginxManager(
    config.network["host"],
    config.network["port"],
    config.network["najdispoj_port"],
    dynamic_data_port=config.network["dynamic_data_port"],
)

dynamic_data_service.register_provider(
    "DPB",
    DPBDynamicDataProvider("0.0.0.0", config.network["dynamic_data_port"], static_data_service.get_provider("DPB").get_folder()),
)
dynamic_data_service.register_provider(
    "OTP2",
    OTP2DynamicDataProvider(otp2_manager.get_instance_address())
)

@app.on_event("startup")
def init_app():
    def update_geodata():
        geodata_service.load_data()
        otp2_manager.build_street_graph()

    def update_static_data():
        gtfs_folders = static_data_service.load_data()
        otp2_manager.build_graph(gtfs_folders)

    scheduler = BackgroundScheduler()
    scheduler.add_job(update_geodata, "cron", day_of_week="sun", hour="2")
    scheduler.add_job(update_static_data, "cron", hour="4")
    scheduler.start()

    update_geodata()
    update_static_data()
    otp2_manager.serve(restart=True)

    nginx_manager.build_client()
