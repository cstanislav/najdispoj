export default {
  title: "Najdispoj Olomouc",
  ticket: false,
  api: {
    host: "http://localhost:8081/api",
  },
  locales: {
    default: "cs",
    fallback: "en",
    supported: [
      {
        code: "cs",
        name: "Čeština"
      },
      {
        code: "sk",
        name: "Slovenčina"
      },
      {
        code: "en",
        name: "English"
      },
    ],
  },
  map: {
    vehicles: true,
    vehiclesUrl: "",
    center: {
      lat: 49.590628,
      lng: 17.256722
    },
    zoom: 14,
  },
  sources: [
    {
      name: "Dopravní podnik města Olomouce, a.s.",
      url: "https://www.dpmo.cz/",
    },
    {
      name: "OpenStreetMap contributors",
      url: "https://www.openstreetmap.org/",
    },
  ],
};
