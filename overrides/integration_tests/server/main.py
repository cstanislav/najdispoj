from apscheduler.schedulers.background import BackgroundScheduler

from server.init import (
    geodata_service,
    geocoding_service,
    static_data_service,
    routing_service,
    app,
)
from server.providers.geodata.overpass_api import OverpassAPIGeodataProvider
from server.providers.routing.opentripplanner2 import OpenTripPlanner2RoutingProvider
from server.providers.geocoding.photon import PhotonGeocodingProvider
from server.providers.agencies.dpmo.static_data import DPMOStaticDataProvider
from server.containers.nginx.manager import NginxManager
from server.containers.opentripplanner2.manager import OpenTripPlanner2Manager
from server.models.bounding_box import BoundingBox
from server.models.coords import Coords

import server.config as config

import server.testing as testing
from datetime import datetime

geodata_service.register_provider(
    "Olomouc",
    OverpassAPIGeodataProvider(
        "/mnt/data/osm/olomouc.osm.pbf",
        BoundingBox(Coords(49.35, 17), Coords(49.75, 17.5)),
    )
)
geocoding_service.register_provider("Photon", PhotonGeocodingProvider("https://photon.komoot.io"))
static_data_service.register_provider("DPMO", DPMOStaticDataProvider("/mnt/data/gtfs/dpmo/"))

otp2_manager = OpenTripPlanner2Manager(routing_service)

routing_service.register_provider("OpenTripPlanner 2", OpenTripPlanner2RoutingProvider(otp2_manager.get_instance_address()))

nginx_manager = NginxManager(
    config.network["host"],
    config.network["port"],
    config.network["najdispoj_port"],
    dynamic_data_port=config.network["dynamic_data_port"],
)

@app.on_event("startup")
def init_app():
    def update_geodata():
        geodata_service.load_data()
        otp2_manager.build_street_graph()

    def update_static_data():
        gtfs_folders = static_data_service.load_data()
        otp2_manager.build_graph(gtfs_folders)

    scheduler = BackgroundScheduler()
    scheduler.add_job(update_geodata, "cron", day_of_week="sun", hour="2")
    scheduler.add_job(update_static_data, "cron", hour="4")
    scheduler.start()

    update_geodata()
    update_static_data()
    otp2_manager.serve(restart=True)
    nginx_manager.build_client()

    # Schedule the job
    scheduler.add_job(testing.run_integration_tests, 'date', run_date=datetime.now())