import requests

def test_autocomplete_should_exist():
    """Makes a GET request to http://najdispoj-nginx:8081/api/autocomplete?query=fibichova+10+olo"""

    response = requests.get(
        'http://najdispoj-nginx:8081/api/autocomplete',
        params={
            'query': 'fibichova 10 olo'
        }
    )
    response_body = response.json()

    assert response.status_code == 200

    assert 'query' in response_body
    assert response_body['query'] == 'fibichova 10 olo'

    assert 'places' in response_body
    assert len(response_body['places']) >= 1

    assert 'coords' in response_body['places'][0]
    assert 'lat' in response_body['places'][0]['coords']
    assert 'lng' in response_body['places'][0]['coords']

    assert 'name' in response_body['places'][0] 
    assert response_body['places'][0]['name']['primary'] == 'Fibichova 10'
    assert response_body['places'][0]['name']['secondary'] == 'Olomouc'

    assert 'type' in response_body['places'][0]
    assert 'modes' in response_body['places'][0]
    assert 'stopIds' in response_body['places'][0]

def test_autocomplete_should_be_empty():
    """Makes a GET request to http://najdispoj-nginx:8081/api/autocomplete?query=asdasdasd"""

    response = requests.get(
        'http://najdispoj-nginx:8081/api/autocomplete',
        params={
            'query': 'asdasdasd'
        }
    )
    response_body = response.json()

    assert response.status_code == 200

    assert 'query' in response_body
    assert response_body['query'] == 'asdasdasd'

    assert 'places' in response_body
    assert len(response_body['places']) == 0