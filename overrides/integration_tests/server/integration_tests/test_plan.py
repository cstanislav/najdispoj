import requests

def test_plan_should_exist():
    """Makes a GET request to http://najdispoj-nginx:8081/api/plan?origin=%7B%22lat%22%3A49.59553536172837%2C%22lng%22%3A17.247186934917668%7D&destination=%7B%22lat%22%3A49.59238158235826%2C%22lng%22%3A17.27741996004378%7D&arriveBy=false&date=2024-03-20&maxTransfers=10&walkReluctance=3&walkSpeed=1&optimize=transfers&time=18%3A17"""

    response = requests.get(
        'http://najdispoj-nginx:8081/api/plan?',
        params={
            'origin': '{"lat":49.59553536172837,"lng":17.247186934917668}',
            'destination': '{"lat":49.59238158235826,"lng":17.27741996004378}',
            'arriveBy': 'false',
            'date': '2024-03-20',
            'maxTransfers': '10',
            'walkReluctance': '3',
            'walkSpeed': '1',
            'optimize': 'transfers',
            'time': '18:17'
        }
    )
    response_body = response.json()

    assert response.status_code == 200

    assert 'plan' in response_body
    assert 'itineraries' in response_body['plan']
    assert len(response_body['plan']['itineraries']) >= 2

    assert 'arrival' in response_body['plan']['itineraries'][0]
    assert 'departure' in response_body['plan']['itineraries'][0]
    assert 'duration' in response_body['plan']['itineraries'][0]
    assert 'legs' in response_body['plan']['itineraries'][0]
    assert 'timelineDuration' in response_body['plan']['itineraries'][0]

    assert len(response_body['plan']['itineraries'][0]['legs']) == 1
    assert response_body['plan']['itineraries'][0]['legs'][0]['mode'] == 'WALK'
    assert len(response_body['plan']['itineraries'][0]['legs'][0]['stops']) == 2

    assert 'arrival' in response_body['plan']['itineraries'][1]
    assert 'departure' in response_body['plan']['itineraries'][1]
    assert 'duration' in response_body['plan']['itineraries'][1]
    assert 'legs' in response_body['plan']['itineraries'][1]
    assert 'timelineDuration' in response_body['plan']['itineraries'][1]

    assert len(response_body['plan']['itineraries'][1]['legs']) >= 3

    assert response_body['plan']['itineraries'][1]['legs'][0]['mode'] == 'WALK'
    assert response_body['plan']['itineraries'][1]['legs'][1]['mode'] != 'WALK'
    assert len(response_body['plan']['itineraries'][1]['legs'][1]['stops']) >= 2

    assert response_body['plan']['itineraries'][1]['legs'][-1]['mode'] == 'WALK'

def test_plan_should_be_empty():
    """Makes a GET request to http://najdispoj-nginx:8081/api/plan?origin=%7B%22lat%22%3A49.51727858680252%2C%22lng%22%3A17.25323558500902%7D&destination=%7B%22lat%22%3A49.668038608784364%2C%22lng%22%3A17.31471080406237%7D&arriveBy=false&date=2024-03-20&maxTransfers=10&walkReluctance=3&walkSpeed=1&optimize=transfers&time=18%3A38"""

    response = requests.get(
        'http://najdispoj-nginx:8081/api/plan',
        params={
            'origin': '{"lat":49.51727858680252,"lng":17.25323558500902}',
            'destination': '{"lat":49.668038608784364,"lng":17.31471080406237}',
            'arriveBy': 'false',
            'date': '2024-03-20',
            'maxTransfers': '10',
            'walkReluctance': '3',
            'walkSpeed': '1',
            'optimize': 'transfers',
            'time': '18:38'
        }
    )
    response_body = response.json()

    assert response.status_code == 200

    assert 'plan' in response_body
    assert 'itineraries' in response_body['plan']
    assert len(response_body['plan']['itineraries']) == 0

def test_plan_should_be_walk_only():
    """Makes a GET request to http://najdispoj-nginx:8081/api/plan?origin=%7B%22lat%22%3A49.591896627870234%2C%22lng%22%3A17.26319528581119%7D&destination=%7B%22lat%22%3A49.6713536607027%2C%22lng%22%3A17.31997551234929%7D&arriveBy=false&date=2024-03-20&maxTransfers=10&walkReluctance=3&walkSpeed=1&optimize=transfers&time=18%3A40"""

    response = requests.get(
        'http://najdispoj-nginx:8081/api/plan',
        params={
            'origin': '{"lat":49.591896627870234,"lng":17.26319528581119}',
            'destination': '{"lat":49.6713536607027,"lng":17.31997551234929}',
            'arriveBy': 'false',
            'date': '2024-03-20',
            'maxTransfers': '10',
            'walkReluctance': '3',
            'walkSpeed': '1',
            'optimize': 'transfers',
            'time': '18:40'
        }
    )
    response_body = response.json()

    assert response.status_code == 200

    assert 'plan' in response_body
    assert 'itineraries' in response_body['plan']
    assert len(response_body['plan']['itineraries']) == 1

    assert 'arrival' in response_body['plan']['itineraries'][0]
    assert 'departure' in response_body['plan']['itineraries'][0]
    assert 'duration' in response_body['plan']['itineraries'][0]
    assert 'legs' in response_body['plan']['itineraries'][0]
    assert 'timelineDuration' in response_body['plan']['itineraries'][0]

    assert len(response_body['plan']['itineraries'][0]['legs']) == 1
    assert response_body['plan']['itineraries'][0]['legs'][0]['mode'] == 'WALK'
    assert len(response_body['plan']['itineraries'][0]['legs'][0]['stops']) == 2
