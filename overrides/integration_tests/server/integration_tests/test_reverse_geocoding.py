import requests

def test_reverse_geocoding_should_exist():
    """Makes a GET request to http://najdispoj-nginx:8081/api/reverse?coords=%7B%22lat%22%3A49.591824033284844%2C%22lng%22%3A17.275391163514005%7D"""

    response = requests.get(
        'http://najdispoj-nginx:8081/api/reverse',
        params={
            'coords': '{"lat":49.591824033284844,"lng":17.275391163514005}'
        }
    )
    response_body = response.json()

    assert response.status_code == 200

    assert 'coords' in response_body
    assert response_body['coords']['lat'] == 49.591824033284844
    assert response_body['coords']['lng'] == 17.275391163514005

    assert 'name' in response_body
    assert 'primary' in response_body['name']
    assert 'secondary' in response_body['name']

    assert 'type' in response_body
    assert 'modes' in response_body
    assert 'stopIds' in response_body

