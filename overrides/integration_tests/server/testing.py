import pytest
import logging

class ResultCollector:
    def __init__(self):
        self.results = []
        self.total = 0
        self.failed = 0

    def pytest_report_teststatus(self, report):
        if report.when == 'call':
            self.total += 1

            if report.outcome == 'failed':
                self.failed += 1
                print("❌ Failed test:", report)

def run_integration_tests():
    logging.info("Running integration tests...")
    tests_path = 'server/integration_tests'

    # Initialize the collector
    collector = ResultCollector()

    # Run pytest
    pytest.main([tests_path], plugins=[collector])

    # Log the results
    logging.info(f"Total tests: {collector.total}")
    logging.info(f"Failed tests: {collector.failed}")

    if collector.failed == 0:
        logging.info("✅ Integration tests passed!")
    else:
        logging.error("❌ Integration tests failed!")