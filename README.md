# 🚍 **Najdispoj**

_The complete open source solution for creating and deploying your custom trip planner._

_Najdispoj_ is a _Docker_-based platform for creating and managing a custom door-to-door trip planner. It hides away the complexity of setting up _Docker_ containers and lets people without deep knowledge of software engineering set up their own trip planner within a few days, if not hours.

**Try it out! ➡️** [https://najdispoj.sk](https://najdispoj.sk)

## Main features:

- Mobile-first, fully responsive modern design.
- Multiple ways of entering the origin and destination points:
  - Map selection 🗺️
  - Address, place name ✍️
  - Geolocation 📡
  - Voice input 🗣️
- Real-time data support (beta)
- Extendable support for various providers of data, with the possibility of combining results from multiple sources
- Modularity, extendability and ease of deployment and development
- Full localization support
- Progressive Web App support

Who is _Najdispoj_ for?

- **Transit providers** - Make your services more accessible and attractive to potential passengers.
- **Public transport enthusiasts** - If your city/region lacks good trip-planning services, you can easily create one yourself using publicly available data.

The main goals of _Najdispoj_ are:

🔴 Making **public transport** as **accessible and attractive** as possible.

🟡 Keeping it **easy to modify, extend and integrate** into other systems.

🔵 Promoting the use of **open source software** and **open data** in the public sphere.

## Planned features

- Dark mode 🌙
- Support for lesser-known country-specific data formats
- Admin dashboard
- Improved accessibility
- Integration of ticket payment systems
- ...

## Basic usage

The following instructions are for running _Najdispoj_ locally with Olomouc public transport data. *Docker* is required.

1. Build the project:

        sudo docker compose build --build-arg override=olomouc

2. Run the project:

        sudo docker compose up

3. Access the project at `http://localhost:8081`

## Complete deployment guide

1. **Get a VPS/dedicated server**

    - At least **40 GB disk space**
      - Depending on the size of your region, more space might be necessary.
    - At least **16 GB RAM**
      - Both disk and RAM requirements are very rough estimates. You might need more or less depending on the size of your region and the number of providers you want to include. Try to monitor the memory usage and adjust the resources accordingly. **If you have plenty of memory and still experience issues (crashes during graph building), make sure Docker is not limiting memory allocation to containers.**
    - _Debian 10/11_ recommended

2. **Connect to the server via SSH as root**

3. **Create user _najdispoj_**

        adduser najdispoj

    - Follow the on-screen instructions.

4. **Install _sudo_**

    - [Instructions](https://unix.stackexchange.com/questions/354928/bash-sudo-command-not-found)
    - Commands:

            apt-get install sudo -y
            usermod -aG sudo najdispoj

    - > **(Optional)** Disable root login via SSH

5. **Log in via SSH as user _najdispoj_**

6. **Install _Docker_**

    - [Instructions part 1](https://docs.docker.com/engine/install/debian/)
    - [Instructions part 2](https://docs.docker.com/engine/install/linux-postinstall/)

7. **Download and configure _Najdispoj_**

    - Clone from this _Git_ repository, or upload manually via FTP/SFTP to folder `/home/najdispoj/najdispoj`
    - Create a new folder inside /overrides (e.g. `/overrides/customconfig`) and copy the contents of `/overrides/olomouc` inside
      - This folder follows the folder structure of the project and any files placed inside will override the default project files on `docker compose build` (see the next step).
    - Configure server
      - Edit files:

        `.env`
        `/overrides/customconfig/server/main.py`
        `/overrides/customconfig/server/config.py`

    - Configure client
      - Edit file:

        `/overrides/customconfig/client/config.ts`

    - If you need to make any other changes, such as use HTTPS instead of HTTP, you can do so by creating a file called `Dockerfile` in `/overrides/customconfig/server/containers/nginx`

8. **Build configured _Najdispoj_ image**

        sudo docker compose build --build-arg override=customconfig [--no-cache]
      
      - **For demonstration purposes, `override=olomouc` is recommended, as it requires no further configuration.**

9. **Run _Najdispoj_**

        sudo docker compose up -d

    - When deploying for the first time, run without _-d_ to see error messages, or check the container log.
    - If you are running Najdispoj on Windows, you might need to increase Docker's memory limit (in `.wslconfig` file) to be able to build the transit graph.

10. **Get an SSL certificate (optional - HTTPS only)**

        sudo apt-get install certbot -y
        sudo apt-get install ufw -y
        sudo ufw allow 80/tcp
        sudo certbot certonly --standalone <parameters>

    - Parameters:
      - Get a certificate without entering email:
        - _--register-unsafely-without-email_
      - Get a test certificate (e.g. when deploying for the first time):
        - _--staging_
      - [Advanced settings](https://eff-certbot.readthedocs.io/en/stable/using.html#certbot-commands)
    - Renew old certificates (Nginx must be stopped):
      - _sudo certbot renew_
        - if Nginx container is running, you must stop it before renewing the certificate, then start it again
        - if this doesn't work, try enabling/disabling `ufw`
          - `sudo ufw enable` / `sudo ufw disable`

11. **(Optional)** Run _Najdispoj_ on server boot and restart on failure

    - Create a file called `najdispoj.service` in `/etc/systemd/system` and paste the following inside:

              [Unit]
              Description=Najdispoj
              After=network.target
              [Service]
              User=najdispoj
              Restart=on-failure
              RestartSec=30
              ExecStart=/home/najdispoj/najdispoj/run_service.sh
              [Install]
              WantedBy=multi-user.target

    - Enter the following command:

            sudo systemctl enable najdispoj

    - You may need to change permissions of the `run_service.sh` file:

            chmod +x /home/najdispoj/najdispoj/run_service.sh

    - Modify build command in `run_service.sh` to include the override from step 7

## Development guide

_Docker_ is required for development.

_Najdispoj_ has been developed in _Visual Studio Code_ inside a development container. It is possible to develop _Najdispoj_ both on local and remote dev container (the latter of which also being useful for troubleshooting while setting up your instance). The configuration files required by _Visual Studio Code_ are included in the repository.

Inside the dev container, run _Najdispoj_ using the following command (`-dev` parameter enables reload-on-save):

    python server/run.py -dev

### Testing
#### Integration tests
To run the integration tests, use Najdispoj with the `integration_tests` override:

    sudo docker compose build --build-arg override=integration_tests
    sudo docker compose up -d

#### Unit tests
Pytest unit tests are located in the `/server/tests` directory.

### Developing the client

Inside the dev container, run the following command from the `/client` directory:

    npm run serve

More commands are described in the README.md file inside the directory.

### Debugging a Najdispoj-managed container

Attach _VS Code_ to the running container and kill the running process (find its process id using _top_ command). Run the process manually with console output (the full command can be found in najdispoj-server's console output).

### Disabling SSL

To disable SSL (e.g. when developing locally), comment out the "https" config at the bottom of `/containers/Nginx/Dockerfile`. You should also change the network port to 80 (or other) in `/server/config.py`.

### Increasing file watchers limit

You might want to increase the system limit for number of file watchers (otherwise the `npm run serve` command could fail). On the host machine (**Important: Not inside a _Docker_ dev container**) open the terminal as user _najdispoj_ and enter the following command:

    echo fs.inotify.max_user_watches=524288 | sudo tee -a /etc/sysctl.conf && sysctl -p

![Object model diagram](static/diagram.svg)
