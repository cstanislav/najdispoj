FROM python:3.10.2

WORKDIR /usr/src/app

ARG AARCH64=true

RUN apt-get update

# Install Docker
RUN	apt-get install ca-certificates curl gnupg lsb-release -y &&\
	mkdir -p /etc/apt/keyrings &&\
	curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg &&\
	echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
	$(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null &&\
	chmod a+r /etc/apt/keyrings/docker.gpg &&\
	apt-get update &&\
	apt-get install docker-ce docker-ce-cli containerd.io docker-compose-plugin -y

# Build & install Pfaedle
ARG PFAEDLE_PATH=/usr/src/app/vendor/pfaedle
RUN apt-get install cmake -y &&\
	git clone --recurse-submodules https://github.com/ad-freiburg/pfaedle ${PFAEDLE_PATH}

# Make Pfaedle work on AArch64 architecture
RUN if [ "${AARCH64}" = "true" ]; then \
	sed -i 's/char c;/int c;/g' ${PFAEDLE_PATH}/src/pfaedle/config/ConfigReader.cpp; \
	sed -i 's/static_cast<int/static_cast<unsigned int/g' ${PFAEDLE_PATH}/src/cppgtfs/src/ad/util/CsvParser.cpp && \
	sed -i 's/static_cast<signed char/static_cast<unsigned char/g' ${PFAEDLE_PATH}/src/cppgtfs/src/ad/util/CsvParser.cpp && \
	sed -i 's/-17/0xEF/g' ${PFAEDLE_PATH}/src/cppgtfs/src/ad/util/CsvParser.cpp && \
	sed -i 's/-69/0xBB/g' ${PFAEDLE_PATH}/src/cppgtfs/src/ad/util/CsvParser.cpp && \
	sed -i 's/-65/0xBF/g' ${PFAEDLE_PATH}/src/cppgtfs/src/ad/util/CsvParser.cpp; \
	fi

WORKDIR ${PFAEDLE_PATH}/build

RUN cmake .. &&\
	make -j &&\
	make install

# Install OSM tools
RUN apt-get install osmctools -y &&\
	apt-get install osmium-tool -y

# Install Node.js
RUN apt-get install npm -y &&\
	npm install npm@10.2.3 -g &&\
	npm install n -g &&\
	n 20.9.0

# pip install would fail without this
RUN apt-get install libgdal-dev -y

WORKDIR /usr/src/app

# Set up Python environment
COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt
COPY . .

ARG override=default
COPY /overrides/${override}/ .

CMD [ "bash", "run_app.sh" ]